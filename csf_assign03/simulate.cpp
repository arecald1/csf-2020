/*
 * This file contains function that will be used to 
 * run through a file and perform the proper loads and stores
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include "simulate.hpp"
#include "cache.h"

using std::vector;
using std::string;
using std::stringstream;
using std::ifstream;
using std::cout;
using std::cin;
using std::endl;
using std::getline;
using std::hex; 

/*
 * Prints the statistics collected by the Cache
 *
 * Parameters:
 * 	cache - a pointer a Cache that holds the stats we want to print
 */
void print_stats(Cache* cache) {
	// Get all stats from the cache
	int load_hits = cache->get_load_hit();
	int load_misses = cache->get_load_miss();
	int store_hits = cache->get_store_hit();
	int store_misses = cache->get_store_miss();
	int total_cycles = cache->get_cycles();
	
	// Print stats in proper format
	cout << "Total loads: " << load_hits + load_misses << endl;
	cout << "Total stores: " << store_hits + store_misses << endl;
	cout << "Load hits: " << load_hits << endl;
	cout << "Load misses: " << load_misses << endl;
	cout << "Store hits: " << store_hits << endl;
	cout << "Store misses: " << store_misses << endl;
	cout << "Total Cycles: " << total_cycles << endl;	
}

/*
 * Takes a power of 2 integer and returns the exponent x of the number
 * when it is in 2^x form
 *
 * Parameters:
 * 	tag_a - an unsigned int reference to the actual tag of the address 
 * 		(that we will "return" from this function)
 *	index_a - an unsigned int reference to the actual index of the address 
 *		(that we will "return" from this function)
 *	address - a string of the hex address 
 *	num_tag_bits - a reference to the number of tag bits in address
 *	num_index_bits - a reference to the number of index bits in address
 *	num_offset_bits - a reference to the number of offset bits in address
 * 
 */
void get_tag_index_from_address(unsigned int& tag_a, unsigned int& index_a, 
		string& address, const int& num_index_bits, const int& 
		num_offset_bits) {
	stringstream convert_to_int;
	unsigned int int_address;

	
	// Convert both hex strings to integer using a stringstream
	convert_to_int << hex << address;
	convert_to_int >> int_address;
	
	// Shift number right by the number of offset bits so we are left with
	// the tag and the index	
	int_address = int_address >> num_offset_bits;

	// Right shift address by index and offset so only the tag is left
	tag_a = int_address >> (num_index_bits);


	// Get the lowest n bits of int_address where n = num_index_bits
	index_a = int_address & ~(~0U << num_index_bits);
}

/*
 * Finds the number of bits for the tag and the index and returns them
 * through the reference parameters
 *
 * Parameters:
 * 	num_sets - an int reference to the number of sets in cache
 * 	blocks_per_set - int referece to the number of blocks per set
 * 	bytes_per_block - int reference to the number of bytes per block
 * 	tag - int reference that represents the number of bits for the tag 
 * 		(that we will "return" from this function)
 * 	index - int reference that represents the number of bits for the index 
 * 		(that we will "return" from this function)
 * 	offset - int reference that represents the number of bits for the 
 * 		offset (that we will "return" from this function)
 */
void tag_and_index_bits(const int& num_sets, const int& bytes_per_block, 
		int& tag, int& index, int& offset) {
	// Log base 2 of bytes per block is the number of offset bits
	// Log base 2 of sets per cache is number of index bits
	// tag is the rest of the bits out of the 32 bit address
	offset = log2(bytes_per_block);
	index = log2(num_sets);
	tag = 32 - offset - index;	
}

/*
 * Reads in lines from file, executes instruction, and prints statistics
 * on the cache simulation
 * 
 * Parameters:
 * 	sets_in_cache - an integer representing the number of sets in the cache 
 * 	blocks_in_set - an integer representing the number of blocks per set
 * 	bytes_in_block - an integer representing the number of bytes per block
 * 	hit_type - a string representing the hit policy
 * 	miss-type - a string representing the miss policy
 * 	evict-type - a string representing the evict policy
 *
 */
void simulate(int sets_in_cache, int blocks_in_set, int bytes_in_block, string
		hit_type, string miss_type, string evict_type) {
	int num_tag_bits = 0;
	int num_index_bits = 0;
	int offset_bits = 0;
	unsigned int tag_a = 0;
	unsigned int index_a = 0;
	string inp_string;
	stringstream ss;
	char action;
	string address;
	
	// Figure out how many bits of the address are reserved for the tag,
	// index, and offset based off of the sets in cache and bytes per block
	tag_and_index_bits(sets_in_cache, bytes_in_block, num_tag_bits, 
			num_index_bits, offset_bits);
	
	// Create a pointer to a cache object
	Cache *cache = new Cache(sets_in_cache, blocks_in_set, bytes_in_block, 
			miss_type, hit_type, evict_type);
	
	// Loop until an empty line is read
	while(getline(cin, inp_string) && !inp_string.empty()){
		// Read in the first two elements in the line, store the 
		// first in action (either l or s) and second in address
		ss << inp_string;
		ss >> action;
		ss >> address;
		
		// Get the actual tag and index from the current address string
		get_tag_index_from_address(tag_a, index_a, address, 
				num_index_bits, offset_bits);
		
		// Simulate the store/load command on the cache
		cache->store_or_load(tag_a, index_a, action);	

		// clears the stringstream
		ss.str(string());	
	}

	// Print the statistics of the simulation
	print_stats(cache);

	delete cache;

}

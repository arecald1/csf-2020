/*
 * This is the class representing a Block
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */
#ifndef BLOCK_H
#define BLOCK_H

class Block {
public:
	Block();
	
	// Returns the valid field describing if the block is valid
	int get_valid() { return valid; }

	// Sets the dirty field to passed in int d
	void set_dirty(int d) { dirty = d; }
	
	// Returns the dirty field
	int get_dirty() { return dirty; }

	void update_tag(int t, char action);
	
	// Returns the tag field
	unsigned int get_tag() { return tag; }

	// updates the timestamp field of the block with int parameter time
	void update_time_stamp(int time) { time_stamp = time; }

	// Returns the timestamp field of the current Block
	int get_time_stamp() { return time_stamp; }

private:
	// valid describes if the block contains a tag
	int valid;
	
	// dirty represents if the block is dirty or not
	int dirty;
	
	// the tag of the address stored in this Block
	unsigned int tag;

	// The time stamp that will be updated every time the block is used
	int time_stamp;
};

#endif

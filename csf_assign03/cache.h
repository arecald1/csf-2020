/*
 * Header file for the cache class
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */

#ifndef CACHE_H
#define CACHE_H

#include <vector>
#include <string>
#include "set.h"


class Cache {
public:
	Cache(int sets, int blocks_per_set, int bytes_per_block, 
			std::string miss, std::string hit, std::string evict);
	
	// The following get methods allows main to access each individual
	// stat field held in the cache
	int get_load_hit() { return load_hit; }

	int get_load_miss() { return load_miss; }

	int get_store_hit() { return store_hit; }

	int get_store_miss() { return store_miss; }

	int get_cycles() { return total_cycles; }

	void store_or_load(const unsigned int& tag, const unsigned int& index,
			const char& action);
private:
	// vector of sets, may need Set::Set
	std::vector<Set> sets;

	// strings that hold the policies for hit, miss, and eviction
	std::string hit_type, miss_type, evict_type;
	
	// integers that hold the statistics we want to collect
	int load_hit, load_miss, store_hit, store_miss, total_cycles;
	
	// Integer representing bytes_per_block
	int bytes_per;

	void update_hit_or_miss_count(const int& outcome_of_command, const 
			char& action);
	
	void load_miss_cycles(const int& outcome);

	void store_miss_cycles(const int& outcome);

	void update_cycle_count(const int& outcome_of_command, 
			const char& action);

	void update_stats(const int& outcome_of_command, const char& action);
};

#endif

/*
 * This is the class representing a Set
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include "set.h"
#include "block.h"
#include <vector>
#include <string>

using std::vector;
using std::string;

/*
 * Creates a set object
 *
 * Parameters:
 * 	blocks_per_set - integer that holds the number of blocks per set
 */
Set::Set(int blocks_per_set) {
	// Creates a vector and pushes a new Block object into it until
	// the correct number of blocks per set is reached
	for (int i = 0; i < blocks_per_set; i++) {
		Block curr = Block();
		slots.push_back(curr);
	}
	
	num_slots = blocks_per_set; 
	lru_count = 1;
	fifo_count = 0;
}

/*
 * Checks if the action was a hit or a miss by searching for the tag, then
 * calls the correct function based on the result to simulate the hit/miss
 *
 * Parameters:
 * 	tag - unsigned int reference to the tag of the address
 * 	miss_type - string reference representing the miss policy
 * 	evict_type - string reference representing the evict policy
 * 	action - char reference to the action executed on the cache (l or s)
 *
 * Return:
 * 	an integer representing the outcome of the action on the cache
 */
int Set::store_or_load(const unsigned int& tag, const string& miss_type, 
		const string& evict_type, const char& action) {
	// if its a hit, we get an index, otherwise, we get -1/-2
	int slot_ind = search(tag);
	
	// If slot_ind is less than 0, it was a miss.
	// Otherwise, it is passed to the hit function and is a valid index
	// to the Block in the vector in this set
	if (slot_ind < 0) {
		// Should simulate the correct miss and then return the
		// type of miss: 0 - cache unmodified, 1 - no blocks 
		// evicted (open block slots), 3 - eviction of clean 
		// block,and 4 - eviction of dirty block
		return miss(tag, slot_ind, miss_type, evict_type, action);
	} else {
		// Simulates the correct hit and returns 2
		return hit(slot_ind, action);
	}

	}

/*
 * Searches for the tag in the vector of Blocks.
 *
 * Parameters:
 * 	tag - unsigned int referece to tag of address
 *
 * Returns:
 * 	an integer that is either an index, or signifies if there are any
 * 	invalid blocks in the set left if the tag was not found
 */
int Set::search(const unsigned int& tag) {
	int empty_slots = 0;

	for (int i = 0; i < num_slots; i++) {
		// Check that there is a tag in the block
		// Then check if the tag in the block matches
		// the tag parameter, if so, return 1
		if (slots.at(i).get_valid() == 1) {
			if (slots.at(i).get_tag() == tag) {
				return i;
			}
		} else {
			empty_slots++;
		}
	}
		
	// Returns -2 if not all blocks are full and block is not in set
	if (empty_slots > 0) {
		return -2;
	}
		
	// Returns -1 if tag parameter was not found in set and all 
	// blocks are valid
	return -1;
}

/*
 * Evicts the oldest block thus following fifo and returns dirty state
 *
 * Parameters:
 * 	tag - unsigned int reference to the tag of the address
 * 	action - char reference to the action executed on the cache (l or s)
 *
 * Returns:
 * 	integer representing the dirty state of the evicted block
 */
int Set::fifo(const unsigned int& tag, const char& action) {
	// Check if fifo tracker has gone through vector once, if so,
	// restart fifo at the beginning of the vecotr
	if (fifo_count == num_slots) {
		fifo_count = 0;
	}
		
	// Get the dirty_bit of the block being evicted before its 
	// replaced. Then, make sure to update the tag and increment fifo
	int dirty_state = slots.at(fifo_count).get_dirty();
	slots.at(fifo_count++).update_tag(tag, action);

	return dirty_state;
}

/*
 * Evicts the least recently used block and returns dirty state. To clarify how
 * the lru works, a lru_count field in each set is incremented on each access,
 * but before it is incremented the current field value is store in the Block's
 * timestamp field. Then, everytime this function is called, is searches for the
 * smallest timestamp in the Blocks.
 *
 * Parameters:
 * 	tag - unsigned reference to the tag address
 * 	action - char reference to the action executed on the cache (l or s)
 *
 * Returns:
 * 	an integer representing the dirty state of the evicted block
 */
int Set::lru(const unsigned int& tag, const char& action) {
	// initialize smallest time and our eviction index with the 
	// first block
	int smallest_time = slots.at(0).get_time_stamp();
	int evict_ind = 0;
	int curr_time;
	
	// Loop through vector and check each block, and store the 
	// smallest time stamp and well as the index of said block
	for (int i = 0; i < num_slots; i++) {
		curr_time = slots.at(i).get_time_stamp();
		if (curr_time < smallest_time) {
			smallest_time = curr_time;
			evict_ind = i;
		}
	}
		
	// Get dirty state of block at the eviction index
	int dirty_state = slots.at(evict_ind).get_dirty();

	// Update the tag at eviction index, then update the "new" block
	// at the eviction index with the most recent time_stamp
	slots.at(evict_ind).update_tag(tag, action);
	slots.at(evict_ind).update_time_stamp(lru_count++);

	return dirty_state;
}


/*
 * Performs the proper miss simulation depedent on the miss type,
 * evict_type, and if there are invalid blocks that we can store a tag in
 *
 * Parameters:
 * 	tag - unsigned int reference to the tag of the address
 * 	slot_ind - int reference to the result of trying to find the
 * 		block tag in the set
 * 	miss_type - string reference to the miss policy
 * 	evict_type - string reference to the evict policy
 * 	action - char reference to the action executed on cache (l or s)
 *
 * Returns:
 * 	integer representing the outcome of the miss
 */
int Set::miss(const unsigned int& tag, int& slot_ind, const string& miss_type,
		const string& evict_type, const char& action) {
	// If miss_type is no-write-allocate and action is a store,
	// immediately return 0.
	// If slot_ind is -2, then there are open slots for the new 
	// block to enter
	// if slot_ind is -1, then all slots have a block and we must
	// determine the proper eviction and perform it
	if (miss_type.compare("no-write-allocate") == 0 && action == 's') {
		return 0;
	} else if (slot_ind == -2) {
		int ind = -1;
		int valid = 1;
		
		// Find first invalid block, then exit loop
		while (valid == 1) {
			ind++;
			valid = slots.at(ind).get_valid();
		}
			
		// Store the tag in the block object at the index found above,
		// then update the timestamp for lru
		slots.at(ind).update_tag(tag, action);
		slots.at(ind).update_time_stamp(lru_count++);
		
		// Return 1 to signify that no eviction but a miss 
		// occured
		return 1;
	} else if (slot_ind == -1) {
		int dirty_state = -1;
		// If direct mapped, num_slots = 1 so eviction policy doesnt
		// matter.
		// Otherwise, it calls the appropriate eviction 
		// function.
		if (num_slots == 1) {
			dirty_state = slots.at(0).get_dirty();
			slots.at(0).update_tag(tag, action);
		} else if (evict_type.compare("fifo") == 0) {
			// returns state of dirty bit after eviction
			dirty_state = fifo(tag, action);
		} else if (evict_type.compare("lru") == 0) {
			// Returns state of dirty bit after eviction
			dirty_state = lru(tag, action);
		}
			
		// Returns 3 if block is not dirty and 4 if block was 
		// dirty
		return dirty_state + 3;
	}
	
	return -1;
}

/* 
 * Simulates the correct hit reponse. Updates time stamp and dirty bit but
 * only if the action is a store
 *
 * Parameters:
 * 	slot_ind - int reference to the block we want to access
 * 	action - char reference to action executed on cache (l or s)
 *
 * Returns:
 * 	2 to signify that the outcome of the action on the block was a hit
 */
int Set::hit(int& slot_ind, const char& action) {
	// Increment lru timestamp
	slots.at(slot_ind).update_time_stamp(lru_count++);
	
	// If its a store hit, make the block dirty
	if (action == 's') {
		slots.at(slot_ind).set_dirty(1);
	}
	
	// Return outcome code 2 because it was a hit	
	return 2; 
}



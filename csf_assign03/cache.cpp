/*
 * Cache class
 * Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */
#include <vector>
#include <string>
#include "cache.h"
#include "set.h"

using std::string;
using std::vector;

/*
 * Constructor that creates a new cache object
 *
 * Parameters:
 * 	sets_in_cache - integer representing number of sets in class
 * 	blocks_per_set - integer representing number of blocks per set
 * 	bytes_per_block - integer representing number of bytes per block
 * 	miss - string representing the miss policy
 * 	hit - string representing the hit policy
 * 	evict - string representing the eviction policy
 */
Cache::Cache(int sets_in_cache, int blocks_per_set, int bytes_per_block, 
		string miss, string hit, string evict) {
	// Create the correct number of sets in the cache
	for (int i = 0; i < sets_in_cache; i++) {
		Set curr = Set(blocks_per_set);
		sets.push_back(curr);
	}
	
	// Initialize all cache fields
	bytes_per = bytes_per_block;
	hit_type = hit;
	miss_type = miss;
	evict_type = evict;
	load_hit = 0;
	load_miss = 0;
	store_hit = 0;
	store_miss = 0;
	total_cycles = 0;
}

/*
 * Simulates a load or store on the cache by calling another load or store
 * function on the correct set
 *
 * Parameters:
 * 	tag - unsigned int reference to the address tag
 * 	index - unsigned int reference to the address index
 * 	action - char reference to the action to execute (load or store)
 */
void Cache::store_or_load(const unsigned int& tag, const unsigned int& index, 
		const char& action) {
	int value = -1;

	// Call another load or store simulation on the correct set, return int
	// code that indicates what the result of load or store was
	value = sets.at(index).store_or_load(tag, miss_type, evict_type, 
			 action);

	// Update the cache statistics using the action that was executed and 
	// the outcome of the execution
	update_stats(value, action);
}
	
/*
 * Updates the hit and miss count fields in the cache depending on the outcome
 * of the action (load or store) executed on the cache
 *
 * Parameters:
 * 	outcome_of_command - int reference to the outcome of the load/store
 * 	action - char reference to action executed on the cache
 */
void Cache::update_hit_or_miss_count(const int& outcome_of_command, 
		const char& action) {
	// If it was a store, enter the store field editing branch.
	// If it was a load, enter the load field editing branch
	// If the outcome is a 2, then it was a hit, otherwise it was a miss
	if (action == 's') {
		if (outcome_of_command == 2) {
			store_hit++;
		} else {
			store_miss++;
		}
	} else {
		if (outcome_of_command == 2) {
			load_hit++;
		} else {
			load_miss++;
		}
		
	}
}

/*
 * Uses the outcome of the load miss and increases the cycles accordingly
 *
 * Parameters:
 * 	outcome - integer reference to the outcome code of the load execution
 */
void Cache::load_miss_cycles(const int& outcome) {
	// if outcome is 1, then no eviction occured for this load miss
	// if outcome is 3, then eviction of a clean block occurred
	// if outcome is 4, then eviction of a dirty block occurred
	if (outcome == 1 || outcome == 3) {
		// All loads will be the same miss cost unless a dirty block
		// was evicted under a write-back policy
		
		total_cycles += (25 * bytes_per) + 1; 
	} else if (outcome == 4 && hit_type.compare("write-back") == 0) {
		// cost of storing the dirty block to memory
		total_cycles += (25 * bytes_per);
		
		// cost of loading new block to memory and then loading the 
		// desired value
		total_cycles += (25 * bytes_per) + 1;
	}
}

/*
 * Takes in the outcome of the store miss and increases the cycles accordingly
 *
 * Parameters:
 * 	outcome - integer reference to the outcome code of the store execution
 */
void Cache::store_miss_cycles(const int& outcome) {
	// if outcome is 0, then it was a no-write-allocate miss
	// if outcome is 1, then no eviction occured for this store miss.
	// Outcome cannot equal 2 because that condition is caught outside of
	// this function.
	// if outcome is 3, then eviction of a clean block occurred.
	// if outcome is 4, then eviction of a dirty block occurred.
	if (outcome <= 3 || (outcome == 4 && 
				hit_type.compare("write-through") == 0)) { 
		if (miss_type.compare("write-allocate") == 0) {
			// This statement will happen in all w-allocate cases
			total_cycles += (25 * bytes_per);

			// w-allocate, w-through, dirty/clean/no eviction
			if (hit_type.compare("write-through") == 0) {
				total_cycles += 101;
			} else {
				// w-allocate, w-back, clean/no eviction
				total_cycles++;
			}
		} else {
			// no-w-allocate, write-through, dirty/clean/no evict
			total_cycles += 100;
		}
	} else if (outcome == 4 && hit_type.compare("write-back") == 0) {
		// This branch is purely w-allocate, w-back, dirty eviction

		// Cost of storing the dirty block to memory
		total_cycles += (25 * bytes_per);
		
		// Cost of loading new block to memory and then storing the 
		// desired value in the cache
		total_cycles += (25 * bytes_per) + 1;
	}
}

/*
 * Updates the cycles depending on the outcome of the action executed on the
 * cache. This function handles hits and calls specific functions for misses
 *
 * Parameters:
 * 	outcome_of_command - int reference to the outcome of the action
 * 	action - char reference to the action executed on the cache (l or s)
 */
void Cache::update_cycle_count(const int& outcome_of_command, 
		const char& action) {

	// If outcome is 2, it is a hit.
	// Otherwise, its a miss, so check the action and call the 
	// corresponding function 
	if (outcome_of_command == 2) {
		total_cycles += 1;
		
		// The following executes only for a write-through store hit
		if (hit_type.compare("write-through") == 0 && action == 's') {
			total_cycles += 100;
		}
	} else if (action == 'l') {
		load_miss_cycles(outcome_of_command);
	} else if (action == 's') {
		store_miss_cycles(outcome_of_command);
	}
}
	
/*
 * Calls a function that updates the hit/miss count. Then calls a function that
 * updates the cycle count.
 *
 * Parameters:
 * 	outcome_of_command - int reference to outcome of action execution
 * 	action - char reference to action executed on cache (s or l)
 */
void Cache::update_stats(const int& outcome_of_command, const char& action) {
	// Update hit/miss count
	update_hit_or_miss_count(outcome_of_command, action);
	
	// Update cycle count
	update_cycle_count(outcome_of_command, action);
}


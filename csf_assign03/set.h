/*
 * This is the class header representing a Set
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */
#ifndef SET_H
#define SET_H

#include "set.h"
#include "block.h"
#include <vector>
#include <string>

class Set {
public:
	Set(int blocks_per_set);

	int store_or_load(const unsigned int& tag, const std::string& miss_type, 
			const std::string& evict_type, const char& action);

private:
	std::vector<Block> slots;

	// num_slots is the number of blocks per set
	int num_slots;
	
	// lru holds an integer that is essentially a rolling sum of the 
	// operations on the cache. Everytime a block is accessed, its time 
	// stamp is set to the current sum. 
	int lru_count;

	// fifo holds an index that increases everytime a fifo eviction occurs.
	int fifo_count;
	
	int search(const unsigned int& tag);

	int fifo(const unsigned int& tag, const char& action);

	int lru(const unsigned int& tag, const char& action);

	int miss(const unsigned int& tag, int& slot_ind, const std::string& 
			miss_type, const std::string& evict_type, 
			const char& action);

	int hit(int& slot_ind, const char& action);

};

#endif

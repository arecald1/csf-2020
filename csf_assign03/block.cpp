/*
 * This is the class representing a Block
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include "block.h"

/*
 * Block Contructor that creates a block object. Initializes all fields to 0.
 */
Block::Block() : valid(0), dirty(0), tag(0), time_stamp(0) { }

/*
 * Updates the tag of this block as well as this block's dirty state
 *
 * Parameters:
 * 	t - integer representing the tag to store in the block
 * 	action - char representing the action being preformed on block
 *
 */
void Block::update_tag(int t, char action) { 
	// Update tag field with new tag
	tag = t;

        // if valid is 0, make it 1	
	if (!valid) {
		valid = 1;
	}
		
	// If dirty is 0 and we are storing, make it 1
	// else if dirty is 1 and we are loading a new
	// block, then make dirty 0
	if (!dirty && action == 's') {
		dirty = 1;
	} else if (dirty && action == 'l') {
		dirty = 0;
	}
}


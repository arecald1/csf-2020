// main file that reads in input

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::cerr;
using std::endl;
using std::stringstream;
using std::string;
using std::vector;

void convert_to_int(char* argv[], int *first_int, int *second_int, 
		int *third_int) {
	stringstream input_conversion;

	input_conversion << argv[1];
	input_conversion >> *first_int;
	input_conversion.clear();

	input_conversion << argv[2];
	input_conversion >> *second_int;
	input_conversion.clear();

	input_conversion << argv[3];
	input_conversion >> *third_int;
	input_conversion.clear();
}

int store_string_input(char** argv, string *hit_type, string *miss_type, 
		string *evict_type) {
	string hit_holder(argv[5]);
	string miss_holder(argv[4]);
	string evict_holder(argv[6]);

	*hit_type = hit_holder;
	*miss_type = miss_holder;
	*evict_type = evict_holder;

	if (hit_holder.compare("write-back") && 
			miss_holder.compare("no-write-allocate")) {
		return 5;
	}

	return 0;
}

bool is_power_of_two(int num) {
	if (num == 0) {
		return false;
	}

	while (num != 1) {
		if (num % 2 != 0) {
			return false;
		}

		num /= 2;
	}

	return true;
}

int test_valid_input(int sets, int blocks, int bytes) {
	// Place all parameters into a vector
	vector<int> input_values { sets, blocks, bytes };
	
	// Loop through parameters to check if each are a power of two
	for (int i = 0; i < 3; i++) {
		if (!is_power_of_two(input_values.at(i))) {
			return i + 1;
		}
	}
	
	// Check if bytes is at least 4
	if (bytes < 4) {
		return 4;
	}

	return 0;
}

void print_error_message(int error_code) {
	// Check error code and print the corresponding error message
	switch (error_code) {
		case 1:
			cerr << "Sets in cache are not a power of 2" << endl;
			break;
		case 2:
			cerr << "Blocks in set are not a power of 2" << endl;
			break;
		case 3:
			cerr << "Bytes in block are not a power of 2" << endl;
			break;
		case 4:
			cerr << "Must be at least 4 bytes per block" << endl;
			break;
		case 5:
			cerr << "Cannot have no-write-allocate and write-back" 
				<< endl;
			break;
		case 6:
			cerr << "Incorrect number of command line arguments" 
				<< endl;
			break;
		default:
			cerr << "An error has occured" << endl;
	}
}

int main(int argc, char* argv[]) {
	int sets_in_cache = 0;
	int blocks_in_set = 0;
	int bytes_in_block = 0;
	int error_state = 0;
	string hit_type;
	string miss_type;
	string evict_type;
	
	if (argc != 7) {
		print_error_message(6);
		return 6;
	}

    	convert_to_int(argv, &sets_in_cache, &blocks_in_set, 
			&bytes_in_block);
	
	error_state = test_valid_input(sets_in_cache, blocks_in_set, 
			bytes_in_block);

	if (error_state != 0) {
		print_error_message(error_state);
		return error_state;
	}

	error_state = store_string_input(argv, &hit_type, &miss_type, 
			&evict_type);
	
	if (error_state != 0) {
		print_error_message(error_state);
		return error_state;
	}
	
	/* Need to implement
	simulate(sets_in_cache, blocks_in_set, bytes_in_block, hit_type, 
			miss_type, evict_type);
	*/

	return 0;
}

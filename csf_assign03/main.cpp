/* main file that reads in command line argumets, checks them, store them,
 * and calls the simulation
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */ 

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "simulate.hpp"

using std::cout;
using std::cerr;
using std::endl;
using std::stringstream;
using std::string;
using std::vector;

/*
 * Takes command line integers and stores them in integer variables in main
 * through pointers
 *
 * Parameters:
 * 	first_int - int pointer to integer in main
 * 	second_int - int pointer to another integer in main
 * 	third_int - int pointer to another integer in main
 */
void convert_to_int(char* argv[], int *first_int, int *second_int, 
		int *third_int) {
	// Create a string stream for easy conversion
	stringstream input_conversion;

	// Store the first command line arg in the string stream.
	// Output from the string stream to the pointer pointing to an integer
	// in main. Clear the string stream. Do this for each int parameter
	input_conversion << argv[1];
	input_conversion >> *first_int;
	input_conversion.clear();

	input_conversion << argv[2];
	input_conversion >> *second_int;
	input_conversion.clear();

	input_conversion << argv[3];
	input_conversion >> *third_int;
	input_conversion.clear();
}

/*
 * Checks each string for valid input
 *
 * Parameters:
 * 	hit - string representing the hit policy
 * 	miss - string representing the miss policy
 * 	evict - string representing the evict policy
 *
 * Returns:
 * 	an integer representing the error code, 0 if no error
 */
int check_string(string& hit, string& miss, string& evict) {
	// Check for valid hit policy
	if (hit.compare("write-through") != 0 && 
			hit.compare("write-back") != 0) {
		return 7;
	}
	
	// Check for valid miss policy
	if (miss.compare("write-allocate") != 0 && 
			miss.compare("no-write-allocate") != 0) {
		return 6;
	}

	// Check for valid evict policy
	if (evict.compare("lru") != 0 && evict.compare("fifo") != 0) {
		return 8;
	}

	return 0;
}

/*
 * Takes the command line string arguments and stores them in strings,
 * then checks to make sure they are valid
 *
 * Parameters:
 * 	argv - a char** of the command line inputs
 * 	hit_type - a string to be filled with the hit policy
 * 	miss_type - a string to be filled with the miss policy
 * 	evict_type - a string to be filled with the evict_policy
 *
 * Returns:
 * 	an integer with the error code, 0 is no errors
 */
int store_string_input(char** argv, string *hit_type, string *miss_type, 
		string *evict_type) {
	// Create strings constructed with the command line strings
	string hit_holder(argv[5]);
	string miss_holder(argv[4]);
	string evict_holder(argv[6]);

	// Store the strings that have just been created into their 
	// corresponding string variable pointers that point to strings in main
	*hit_type = hit_holder;
	*miss_type = miss_holder;
	*evict_type = evict_holder;
	
	// Check if the hit and miss policies combination is valid
	if (hit_holder.compare("write-back") == 0 && 
			miss_holder.compare("no-write-allocate") == 0) {
		return 5;
	}

	// Check that all the strings are valid to begin with
	return check_string(hit_holder, miss_holder, evict_holder);
}

/*
 * Checks if an integer is a power of 2
 *
 * Parameters:
 * 	num - an integer that was input from the command line 
 *
 * Returns:
 * 	returns a boolean that says if the int is a power of 2
 */
bool is_power_of_two(int num) {
	// If the integer is 0, return false immediately
	if (num == 0) {
		return false;
	}
	
	// Loop until the number equals 1
	while (num != 1) {
		// Check if the number is divisible by 2, if not, return false
		if (num % 2 != 0) {
			return false;
		}
		
		// Divide the number by 2
		num /= 2;
	}
	
	// If this point is reached, then the number is a power of 2
	return true;
}

/*
 * Test the input arguments integers to ensure they're valid
 *
 * Parameters:
 * 	sets - an integer representings number of sets in cache
 * 	blocks - integer representing number of blockers per set
 * 	bytes - integer representing number of bytes per block
 *
 * Returns:
 * 	an integer error code, 0 if no error
 */
int test_valid_input(int sets, int blocks, int bytes) {
	// Place all parameters into a vector
	vector<int> input_values { sets, blocks, bytes };
	
	// Loop through parameters to check if each are a power of two
	for (int i = 0; i < 3; i++) {
		if (!is_power_of_two(input_values.at(i))) {
			return i + 1;
		}
	}
	
	// Check if bytes is at least 4
	if (bytes < 4) {
		return 4;
	}

	return 0;
}

/*
 * Switch statement that prints the proper message corresponding to the error
 * code
 * 
 * Parameters:
 * 	error_code - integer error code that has a corresponding error message
 */
void print_error_message(int error_code) {
	// Check error code and print the corresponding error message
	switch (error_code) {
		case 1:
			cerr << "Sets in cache are not a power of 2" << endl;
			break;
		case 2:
			cerr << "Blocks in set are not a power of 2" << endl;
			break;
		case 3:
			cerr << "Bytes in block are not a power of 2" << endl;
			break;
		case 4:
			cerr << "Must be at least 4 bytes per block" << endl;
			break;
		case 5:
			cerr << "Cannot have no-write-allocate and write-back" 
				<< endl;
			break;
		case 6:
			cerr << "Invalid write_miss type" << endl;
			break;
		case 7:
			cerr << "Invalid write_hit type" << endl;
			break;
		case 8:
			cerr << "Invalid eviction type" << endl;
			break;
		case 9:
			cerr << "Incorrect number of command line arguments" 
				<< endl;
			break;
		default:
			cerr << "An error has occured" << endl;
	}
}

int main(int argc, char* argv[]) {
	int sets_in_cache = 0;
	int blocks_in_set = 0;
	int bytes_in_block = 0;
	int error_state = 0;
	string hit_type;
	string miss_type;
	string evict_type;
	
	// Check for the correct number of command line arguments	
	if (argc != 7) {
		print_error_message(9);
		return 9;
	}
	
	// Store the command line integers 
    	convert_to_int(argv, &sets_in_cache, &blocks_in_set, 
			&bytes_in_block);
	
	// Check if there is an error with the command line integers
	error_state = test_valid_input(sets_in_cache, blocks_in_set, 
			bytes_in_block);
	
	// Check if error has occured and throw proper error message and code
	if (error_state != 0) {
		print_error_message(error_state);
		return error_state;
	}
	
	// Check the string arguments and store them
	error_state = store_string_input(argv, &hit_type, &miss_type, 
			&evict_type);
	
	// Check if error has occured and throw proper error message and code
	if (error_state != 0) {
		print_error_message(error_state);
		return error_state;
	}
	
	// Run cache simulation
	simulate(sets_in_cache, blocks_in_set, bytes_in_block, hit_type, 
			miss_type, evict_type);

	return 0;
}

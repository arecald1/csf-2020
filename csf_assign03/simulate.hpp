/*
 * Header file for cpp file that simulates the cache
 * CSF Assignment 3
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <string>
#include "cache.h"

void print_stats(Cache* cache);

void get_tag_index_from_address(unsigned int& tag_a, unsigned int& index_a, 
		std::string& address, const int&
		num_index_bits, const int& num_offset_bits);

void tag_and_index_bits(const int& num_sets, const int& bytes_per_block, 
		int& tag, int& index, int& offset);

void simulate(int sets_in_cache, int blocks_in_set, int bytes_in_block, 
		std::string hit_type, std::string miss_type, std::string 
		evict_type);

/*
 * CSF Assignment 1
 * Arbitrary-precision integer data type
 * Header file
 * Andres Recalde
 * arecald1@jhu.edu
 */

#ifndef APINT_H
#define APINT_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct {
	uint64_t* Ap_num_array;
	size_t  Ap_size;
} ApInt;


/* Constructors and destructors */
ApInt *apint_create_from_u64(uint64_t val);
ApInt *apint_create_from_hex(const char *hex);
void apint_destroy(ApInt *ap);

/* Operations */
uint64_t hex_char_to_int(int hex_char);
size_t get_hex_length(const char* hex);
bool check_hex_char(char hex_char);
bool check_hex(const char* hex, size_t hex_len);
int bits_in_hex(char most_sig);
size_t count_bits_hex(const char *hex);
uint64_t apint_get_bits(ApInt *ap, unsigned n);
int apint_highest_bit_set(ApInt *ap);
ApInt *apint_lshift(ApInt *ap);
ApInt *set_shifted_array(ApInt* org_ap, size_t move);
uint64_t check_leading_bits(uint64_t index_num, unsigned n);
ApInt *apint_lshift_n(ApInt *ap, unsigned n);
void shift_hex(char *full_hex);
void append_hex(char *hex_full, char *hex_part, size_t hex_length, int ind_mult);
char convert_to_hex(uint64_t integer);
void parse_ap(char *hex_trans, uint64_t integer);
char *apint_format_as_hex(ApInt *ap);
ApInt *apint_add(const ApInt *a, const ApInt *b);
size_t check_leading_zeroes(uint64_t *diff_array, size_t diff_size);
ApInt *apint_sub(const ApInt *a, const ApInt *b);
int apint_compare(const ApInt *left, const ApInt *right);

#endif /* APINT_H */

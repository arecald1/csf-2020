/*
 * Unit tests for arbitrary-precision integer data type
 *
 * These tests are by no means comprehensive.  You will need to
 * add more tests of your own!  In particular, make sure that
 * you have tests for more challenging situations, such as
 *
 * - large values
 * - adding/subtracting/comparing values with different lengths
 * - special cases (carries when adding, borrows when subtracting, etc.)
 * - etc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "apint.h"
#include "tctest.h"
#include <math.h>

typedef struct {
	ApInt *ap0;
	ApInt *ap1;
	ApInt *ap110660361;
	ApInt *max1;
	/* TODO: add additional fields of test fixture */
	ApInt *fromHex;
	ApInt *largeHex1;
	ApInt *largeHex2;
	ApInt *largeHex3;
	ApInt *largeHex4;
} TestObjs;

TestObjs *setup(void);
void cleanup(TestObjs *objs);

void testCreateFromU64(TestObjs *objs);
void testHighestBitSet(TestObjs *objs);
void testLshiftN(TestObjs *objs);
void testCompare(TestObjs *objs);
void testFormatAsHex(TestObjs *objs);
void testAdd(TestObjs *objs);
void testSub(TestObjs *objs);
/* TODO: add more test function prototypes */
void testCreateFromHex(TestObjs *objs);
void testGetBits(TestObjs *objs);
void testLshift(TestObjs *objs);

int main(int argc, char **argv) {
	TEST_INIT();

	if (argc > 1) {
		/*
		 * name of specific test case to execute was provided
		 * as a command line argument
		 */
		tctest_testname_to_execute = argv[1];
	}

	TEST(testCreateFromU64);
	TEST(testHighestBitSet);
	TEST(testLshiftN);
	TEST(testCompare);
	TEST(testFormatAsHex);
	TEST(testAdd);
	TEST(testSub);
	TEST(testCreateFromHex);
	TEST(testGetBits);
	TEST(testLshift);

	TEST_FINI();
}

TestObjs *setup(void) {
	TestObjs *objs = malloc(sizeof(TestObjs));
	objs->ap0 = apint_create_from_u64(0UL);
	objs->ap1 = apint_create_from_u64(1UL);
	objs->ap110660361 = apint_create_from_u64(110660361UL);
	objs->max1 = apint_create_from_u64(0xFFFFFFFFFFFFFFFFUL);
	objs->fromHex = apint_create_from_hex("aac3e2200444bfccc30e45012b005a0bdeb0e520674f90f029031b623a5a55e92871b3c5d6c836e8e6947");
	// In decreasing order such that largeHex1 is the largest value. 
	objs->largeHex1 = apint_create_from_hex("ddd3264b92cfb278f18a0711033c622e6b697f6269dc92461156181e0cfe20748c35a611ab41f2bb92363f33fb142def4e0");
	objs->largeHex2 = apint_create_from_hex("b158c4c26b9e3ee8ea3ef48c5c17045aa590df70cddeda523ad0348aab7021cc1c3e716b709f4ff1e0f75d76f29191");
	objs->largeHex3 = apint_create_from_hex("488ad335947c9fd9e63928b60bdf6f359bb0f4eac900d0a97479696e6fe7fe76b35c92bfdbdcb52a6dabb7803ff");
	objs->largeHex4 = apint_create_from_hex("28b6d494c717d081b50bb9dc96987671bddf61c8b15c982b35f579a3a079e982350f58913a949dde606");
	return objs;
}

void cleanup(TestObjs *objs) {
	apint_destroy(objs->ap0);
	apint_destroy(objs->ap1);
	apint_destroy(objs->ap110660361);
	apint_destroy(objs->max1);
	apint_destroy(objs->fromHex);
	apint_destroy(objs->largeHex1);
	apint_destroy(objs->largeHex2);
	apint_destroy(objs->largeHex3);
	apint_destroy(objs->largeHex4);
	free(objs);
}

void testCreateFromU64(TestObjs *objs) {
	ASSERT(0UL == apint_get_bits(objs->ap0, 0));
	ASSERT(1UL == apint_get_bits(objs->ap1, 0));
	ASSERT(110660361UL == apint_get_bits(objs->ap110660361, 0));
	ASSERT(0xFFFFFFFFFFFFFFFFUL == apint_get_bits(objs->max1, 0));
}

void testCreateFromHex(TestObjs *objs) {
	char *s;
	ApInt * hex_ap;
	
	// Test creating an ApInt from a large hex
	ASSERT(0 == strcmp("aac3e2200444bfccc30e45012b005a0bdeb0e520674f90f029031b623a5a55e92871b3c5d6c836e8e6947", s = apint_format_as_hex(objs->fromHex/*hex_test*/)));
	free(s);
	
	hex_ap = apint_create_from_hex("01ed00ee");
	// Test making an ApInt from a hex < 16 characters
	ASSERT(0 == strcmp("1ed00ee", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);

	// Test making an ApInt from a hex < 16 with a starting zero
	hex_ap = apint_create_from_hex("97257b626a0");
	ASSERT(0 == strcmp("97257b626a0", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);

	// Test making an ApInt from a hex > 16 characters with leading zeroes
	hex_ap = apint_create_from_hex("92a5fc7da623f9865000");
        ASSERT(0 == strcmp("92a5fc7da623f9865000", s = apint_format_as_hex(hex_ap)));
        apint_destroy(hex_ap);
        free(s);

	// Test making an ApInt from a hex = 16 characters
	hex_ap = apint_create_from_hex("cb79dec104a2cd90");
	ASSERT(0 == strcmp("cb79dec104a2cd90", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);
	
	// Test making an ApInt from a hex % 16 == 0 characters
	hex_ap = apint_create_from_hex("4312c76d9072fac5517379d85feae6ac7acd8fcb5dfed06248c5720dc727ecfd");
	ASSERT(0 == strcmp("4312c76d9072fac5517379d85feae6ac7acd8fcb5dfed06248c5720dc727ecfd", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);

	// Test having only one index with a non-zero number
	hex_ap = apint_create_from_hex("000000001");
	ASSERT(0 == strcmp("1", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);

	hex_ap = apint_create_from_hex("00000000d");
	ASSERT(0 == strcmp("d", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);

	hex_ap = apint_create_from_hex("000000040");
	ASSERT(0 == strcmp("40", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);

	hex_ap = apint_create_from_hex("0000000e0");
	ASSERT(0 == strcmp("e0", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);
	
	// Same tests as above except for 16 char numbers and up
	hex_ap = apint_create_from_hex("0000000000000001");
        ASSERT(0 == strcmp("1", s = apint_format_as_hex(hex_ap)));
        apint_destroy(hex_ap);
        free(s);

        hex_ap = apint_create_from_hex("000000000000000d");
        ASSERT(0 == strcmp("d", s = apint_format_as_hex(hex_ap)));
        apint_destroy(hex_ap);
        free(s);

        hex_ap = apint_create_from_hex("0000000000000040");
        ASSERT(0 == strcmp("40", s = apint_format_as_hex(hex_ap)));
        apint_destroy(hex_ap);
        free(s);

	hex_ap = apint_create_from_hex("00000000000000e0");
	ASSERT(0 == strcmp("e0", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);
	
	hex_ap = apint_create_from_hex("7");
	ASSERT(0 == strcmp("7", s = apint_format_as_hex(hex_ap)));
	apint_destroy(hex_ap);
	free(s);

	hex_ap = apint_create_from_hex("f");
	ASSERT(0 == strcmp("f", s = apint_format_as_hex(hex_ap)));
        apint_destroy(hex_ap);
        free(s);
	
}

void testGetBits(TestObjs *objs) {
	// test with simple values
	// Test an arbitrarily long n
	ASSERT(0 == apint_get_bits(objs->ap110660361, (unsigned) pow(2, 63)));

	// Test with simple values
	ASSERT(0 == apint_get_bits(objs->ap0, 0));
	ASSERT(0 == apint_get_bits(objs->ap0, 259));
	ASSERT(1 == apint_get_bits(objs->ap1, 0));
	ASSERT(0 == apint_get_bits(objs->ap1, 38));
	ASSERT(110660361 == apint_get_bits(objs->ap110660361, 0));
	ASSERT(0 == apint_get_bits(objs->ap110660361, 1));

	// Now check every set of bits in a larger value
	ASSERT(4349752126861109575UL == apint_get_bits(objs->fromHex, 0));
	ASSERT(13124515868762605339UL == apint_get_bits(objs->fromHex, 1));
	ASSERT(5910540174017859633UL == apint_get_bits(objs->fromHex, 2));
	ASSERT(5769867610833152782UL == apint_get_bits(objs->fromHex, 3));
	ASSERT(2450033290444026084UL == apint_get_bits(objs->fromHex, 4));
	ASSERT(699454 == apint_get_bits(objs->fromHex, 5));
	// fromHex is only size 6 so this tests what happens when n > size
	ASSERT(0 == apint_get_bits(objs->fromHex, 6));
	
}

void testHighestBitSet(TestObjs *objs) {
	ASSERT(-1 == apint_highest_bit_set(objs->ap0));
	ASSERT(0 == apint_highest_bit_set(objs->ap1));
	ASSERT(26 == apint_highest_bit_set(objs->ap110660361));
	ASSERT(63 == apint_highest_bit_set(objs->max1));
	
	// Get highest bit for large values
	ASSERT(395 == apint_highest_bit_set(objs->largeHex1));
	ASSERT(375 == apint_highest_bit_set(objs->largeHex2));
	ASSERT(362 == apint_highest_bit_set(objs->largeHex3));
	ASSERT(329 == apint_highest_bit_set(objs->largeHex4));
	
}

void testLshift(TestObjs *objs) {
	// Left shift default small values
	ApInt *holder;
	holder = apint_lshift(objs->ap0);
	ASSERT(0 == apint_get_bits(holder, 0));
	apint_destroy(holder);
	
	holder = apint_lshift(objs->ap1);
	ASSERT(2 == apint_get_bits(holder, 0));
	ASSERT(0 == apint_get_bits(holder, 1));
	apint_destroy(holder);
	
	holder = apint_lshift(objs->ap110660361);
	ASSERT(221320722 == apint_get_bits(holder, 0));
	ASSERT(0 == apint_get_bits(holder, 1));
	apint_destroy(holder);
	
	// Left shifting larger values that are likes to spill over
	holder = apint_lshift(objs->max1);
	ASSERT(0xFFFFFFFFFFFFFFFEUL == apint_get_bits(holder, 0));	
	ASSERT(1 == apint_get_bits(holder, 1));
	apint_destroy(holder);

	holder = apint_lshift(objs->largeHex1);
	ASSERT(14404340519318448576UL == apint_get_bits(holder, 0));
	ASSERT(7262461604821671473 == apint_get_bits(holder, 5));
	apint_destroy(holder);
	
}

void testLshiftN(TestObjs *objs) {
	ApInt *result;

	result = apint_lshift_n(objs->ap0, 17);
	ASSERT(0UL == apint_get_bits(result, 0));
	ASSERT(0UL == apint_get_bits(result, 1));
	apint_destroy(result);

	result = apint_lshift_n(objs->ap1, 17);
	ASSERT(0x20000UL == apint_get_bits(result, 0));
	ASSERT(0UL == apint_get_bits(result, 1));
	apint_destroy(result);

	result = apint_lshift_n(objs->ap110660361, 17);
	ASSERT(0xD3116120000UL == apint_get_bits(result, 0));
	ASSERT(0UL == apint_get_bits(result, 1));
	apint_destroy(result);
}

void testCompare(TestObjs *objs) {
        ApInt * a; 
	ApInt * b;

	
	/* 1 > 0 */
        ASSERT(apint_compare(objs->ap1, objs->ap0) > 0);
        /* 0 < 1 */
        ASSERT(apint_compare(objs->ap0, objs->ap1) < 0);
        /* 110660361 > 0 */
        ASSERT(apint_compare(objs->ap110660361, objs->ap0) > 0);
        /* 110660361 > 1 */
        ASSERT(apint_compare(objs->ap110660361, objs->ap1) > 0);
        /* 0 < 110660361 */
        ASSERT(apint_compare(objs->ap0, objs->ap110660361) < 0);
        /* 1 < 110660361 */
        ASSERT(apint_compare(objs->ap1, objs->ap110660361) < 0);
	// The following tests all test apint_compare's ability to compare large numbers 
	a = apint_create_from_hex("bb523eb76ee4eed55593c09edad632c25000c6e5177b7cdce9a78ba43519830165b1bacf18c960e5062d3f1d");	
	b = apint_create_from_hex("d7c988f8707871");
	ASSERT(apint_compare(a,b) > 0);
	ASSERT(apint_compare(b,a) < 0);
	apint_destroy(a);
	apint_destroy(b);

	a = apint_create_from_hex("37038e20aa22");
	b = apint_create_from_hex("8c416f55b77b45173135317cafe15c19405f31fb6835e8d6d159ee23fdcfd6a015263ea4fd315668fd3bef9437fff6507");
	ASSERT(apint_compare(a,b) < 0);
	ASSERT(apint_compare(b,a) > 0);
	apint_destroy(a);
	apint_destroy(b);

	a = apint_create_from_hex("ce2fedade673d3ef3df49b0307af85023f95");
	b = apint_create_from_hex("1032b9be256645d7fe463ebb6eb0e1558a9107fb3ffc891de919f504668acea301687abec44708b89bfd2cb5748128e17bec");
	ASSERT(apint_compare(a,b) < 0);
	ASSERT(apint_compare(b,a) > 0);
	apint_destroy(a);
	apint_destroy(b);
	
	a = apint_create_from_hex("52f67fb5f49e25e41bd853a5b1738c467bdcdd8a02c0c40b1887590524d4191df76e98eb39d54f413c746a6e03447e23c0c");
	b = apint_create_from_hex("b911b60934ede552340eb");
	ASSERT(apint_compare(a,b) > 0);
	ASSERT(apint_compare(b,a) < 0);
	apint_destroy(a);
	apint_destroy(b);

	// Test how large comparison's work when both values are equal
	a = apint_create_from_hex("52f67fb5f49e25e41bd853a5b1738c467bdcdd8a02c0c40b1887590524d4191df76e98eb39d54f413c746a6e03447e23c0c");
	b = apint_create_from_hex("52f67fb5f49e25e41bd853a5b1738c467bdcdd8a02c0c40b1887590524d4191df76e98eb39d54f413c746a6e03447e23c0c");
	ASSERT(apint_compare(a, b) == 0);
	ASSERT(apint_compare(b, a) == 0);
	apint_destroy(a);
	apint_destroy(b);
	
}

void testFormatAsHex(TestObjs *objs) {
	char *s;

	ASSERT(0 == strcmp("0", (s = apint_format_as_hex(objs->ap0))));
	free(s);

	ASSERT(0 == strcmp("1", (s = apint_format_as_hex(objs->ap1))));
	free(s);

	ASSERT(0 == strcmp("6988b09", (s = apint_format_as_hex(objs->ap110660361))));
	free(s);

	ASSERT(0 == strcmp("ffffffffffffffff", (s = apint_format_as_hex(objs->max1))));
	free(s);
}

void testAdd(TestObjs *objs) {
	ApInt *a;
	ApInt *b;
	ApInt *sum;
	char *s;

	/* 0 + 0 = 0 */
	sum = apint_add(objs->ap0, objs->ap0);
	ASSERT(0 == strcmp("0", (s = apint_format_as_hex(sum))));
	apint_destroy(sum);
	free(s);

	/* 1 + 0 = 1 */
	sum = apint_add(objs->ap1, objs->ap0);
	ASSERT(0 == strcmp("1", (s = apint_format_as_hex(sum))));
	apint_destroy(sum);
	free(s);

	/* 1 + 1 = 2 */
	sum = apint_add(objs->ap1, objs->ap1);
	ASSERT(0 == strcmp("2", (s = apint_format_as_hex(sum))));
	apint_destroy(sum);
	free(s);

	/* 110660361 + 1 = 110660362 */
	sum = apint_add(objs->ap110660361, objs->ap1);
	ASSERT(0 == strcmp("6988b0a", (s = apint_format_as_hex(sum))));
	apint_destroy(sum);
	free(s);

	/* FFFFFFFFFFFFFFFF + 1 = 10000000000000000 */
	sum = apint_add(objs->max1, objs->ap1);
	ASSERT(0 == strcmp("10000000000000000", (s = apint_format_as_hex(sum))));
	apint_destroy(sum);
	free(s);
	
	// Check how value that spans more than one index handles adding 0
	sum = apint_add(objs->fromHex, objs->ap0);
	ASSERT(0 == strcmp("aac3e2200444bfccc30e45012b005a0bdeb0e520674f90f029031b623a5a55e92871b3c5d6c836e8e6947",(s = apint_format_as_hex(sum))));
	apint_destroy(sum);
	free(s);

	// Stress testing add with large ApInt representations:
	
	// fc43e83155e2db5c0972e723af491689d7dfd7814db725eb00bec9ee7cae16e81c9fb6b581bec301d8fc38ff755252a96bd2 + 
	// cdfe0c1cabd8a0a694314f7f6298cf26794297009af4f6bfebb5389d182dc43eea88ea516 = 
	// fc43e83155e2db5c0972e723af55f66a99aa950b582068fff8b4f37b6f15ab118ca96604edbd7e5562cdbbdbb940fb3810e8
	a = apint_create_from_hex("fc43e83155e2db5c0972e723af491689d7dfd7814db725eb00bec9ee7cae16e81c9fb6b581bec301d8fc38ff755252a96bd2");
	b = apint_create_from_hex("cdfe0c1cabd8a0a694314f7f6298cf26794297009af4f6bfebb5389d182dc43eea88ea516");
	sum = apint_add(a,b);
	ASSERT(0 == strcmp("fc43e83155e2db5c0972e723af55f66a99aa950b582068fff8b4f37b6f15ab118ca96604edbd7e5562cdbbdbb940fb3810e8", (s = apint_format_as_hex(sum))));
	apint_destroy(a);
	apint_destroy(b);
	apint_destroy(sum);
	free(s);
	
	// 30dd3cb3a61a953a4a03cc44debcff2033506d15408e8741987c001fc7e82d62 + 
	// 2e667ce870489e0 = 
	// 30dd3cb3a61a953a4a03cc44debcff2033506d15408e87419b6267ee4eecb742
	a = apint_create_from_hex("30dd3cb3a61a953a4a03cc44debcff2033506d15408e8741987c001fc7e82d62");
        b = apint_create_from_hex("2e667ce870489e0");
        sum = apint_add(a,b);
        ASSERT(0 == strcmp("30dd3cb3a61a953a4a03cc44debcff2033506d15408e87419b6267ee4eecb742", (s = apint_format_as_hex(sum))));
        apint_destroy(a);
        apint_destroy(b);
	apint_destroy(sum);
	free(s);

	// ca21d8e28c357815c1da22170248ccece417f608f70b32c23e66774b46888976f161f478926aa26589bf44df07fc756a9381 +
	// 75d17749330a81f3189 =
	// ca21d8e28c357815c1da22170248ccece417f608f70b32c23e66774b46888976f161f478926aa265911c5c539b2d1d89c50a
	a = apint_create_from_hex("ca21d8e28c357815c1da22170248ccece417f608f70b32c23e66774b46888976f161f478926aa26589bf44df07fc756a9381");
        b = apint_create_from_hex("75d17749330a81f3189");
        sum = apint_add(a,b);
        ASSERT(0 == strcmp("ca21d8e28c357815c1da22170248ccece417f608f70b32c23e66774b46888976f161f478926aa265911c5c539b2d1d89c50a", (s = apint_format_as_hex(sum))));
        apint_destroy(a);
        apint_destroy(b);
	apint_destroy(sum);
	free(s);

	// aaf9a92c3e64649eccf302d09a42f1c568535939ef97c3d0f8ada76dac48 +
	// 895a190d62c246bb7cf3c62bfeee06664deec8136405fbdb45a6b94eec12579d5ad69cdaa50aecce5927c1 =
	// 895a190d62c246bb7cf3c62bff99000f7a2d2c7802d2eede1640fc40b17aaaf694c6349e76039a75c6d409
	a = apint_create_from_hex("aaf9a92c3e64649eccf302d09a42f1c568535939ef97c3d0f8ada76dac48");
        b = apint_create_from_hex("895a190d62c246bb7cf3c62bfeee06664deec8136405fbdb45a6b94eec12579d5ad69cdaa50aecce5927c1");
        sum = apint_add(a,b);
        ASSERT(0 == strcmp("895a190d62c246bb7cf3c62bff99000f7a2d2c7802d2eede1640fc40b17aaaf694c6349e76039a75c6d409", (s = apint_format_as_hex(sum))));
        apint_destroy(a);
        apint_destroy(b);
	apint_destroy(sum);
	free(s);

	// Test adding a number to itself
	sum = apint_add(objs->max1, objs->max1);
	ASSERT(0 == strcmp("1fffffffffffffffe", (s = apint_format_as_hex(sum))));
	apint_destroy(sum);
	free(s);

}

void testSub(TestObjs *objs) {
	ApInt *a, *b, *diff;
	char *s;

	/* subtracting 1 from ffffffffffffffff is fffffffffffffffe */
	diff = apint_sub(objs->max1, objs->ap1);
	ASSERT(0 == strcmp("fffffffffffffffe", (s = apint_format_as_hex(diff))));
	apint_destroy(diff);
	free(s);

	/* subtracting 0 from 1 is 1 */
	diff = apint_sub(objs->ap1, objs->ap0);
	ASSERT(0 == strcmp("1", (s = apint_format_as_hex(diff))));
	ASSERT(0 == apint_compare(diff, objs->ap1));
	apint_destroy(diff);
	free(s);

	/* subtracting 1 from 1 is 0 */
	diff = apint_sub(objs->ap1, objs->ap1);
	ASSERT(0 == strcmp("0", (s = apint_format_as_hex(diff))));
	ASSERT(0 == apint_compare(diff, objs->ap0));
	apint_destroy(diff);
	free(s);

	/* subtracting 1 from 0 can't be represented because it is negative */
	ASSERT(NULL == apint_sub(objs->ap0, objs->ap1));

	/* test involving larger values */
	a = apint_create_from_hex("7e35207519b6b06429378631ca460905c19537644f31dc50114e9dc90bb4e4ebc43cfebe6b86d");
	b = apint_create_from_hex("9fa0fb165441ade7cb8b17c3ab3653465e09e8078e09631ec8f6fe3a5b301dc");
	diff = apint_sub(a, b);
	ASSERT(0 == strcmp("7e35207519b6afc4883c6fdd8898213a367d73b918de95f20766963b0251c622cd3ec4633b691",
		(s = apint_format_as_hex(diff))));
	apint_destroy(diff);
	apint_destroy(b);
	apint_destroy(a);
	free(s);
}


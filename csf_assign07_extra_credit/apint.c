/*
 * CSF Assignment 1
 * Arbitrary-precision integer data type
 * Function implementations
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include "apint.h"

/*
 * Creates an ApInt from a uint64_t. Since the parameter is unint64_t, then the
 * number cannot take more than one index of ApInt.
 *
 * Parameters:
 * 	val - a uint64_t value we want represented as an ApInt
 *
 * Returns:
 * 	a pointer to the newly created ApInt representation of the passed in parameter
 */
ApInt *apint_create_from_u64(uint64_t val) {
	uint64_t *new_bit_string = (uint64_t*) malloc(sizeof(uint64_t));
	ApInt * Ap_uint64 = (ApInt*) malloc(sizeof(uint64_t*) + sizeof(size_t));
	new_bit_string[0] = val;
	Ap_uint64->Ap_size = 1;
	Ap_uint64->Ap_num_array = new_bit_string;
	
	return Ap_uint64;
}

/*
 * Converts individual hex characters to the number they represent before 
 * multiplication against the base 16 value. Assumptions are that the hex 
 * is already valid.
 *
 * Parameters:
 * 	hex_char - the direct integer representation of a character
 *
 * Returns:
 * 	the integer value that the character hex is equivalent to 
 */

uint64_t hex_char_to_int(int hex_char) {
	if (hex_char > 47 && hex_char < 58) {
		return hex_char - 48;
	} else if (hex_char < 71) {
		return hex_char - 55;
	} else {
		return hex_char - 87;
	}
}

/*
 * Gets the length of the hexidecimal string does not include '\0'
 * 
 * Parameters:
 * 	hex - a constant character array which should be the hexidecimal
 *
 * Returns:
 * 	a size_t value that contains the length of the hex excluding '\0'
 */
size_t get_hex_length(const char* hex) {
	char char_holder = hex[0];
	size_t hex_size = 0;
	// Loop through until a null pointer is reached and count every loop
	while (char_holder != '\0') {
		hex_size++;
		char_holder = hex[hex_size];
	}
	
	return hex_size;
}

/*
 * Checks if the character is a valid hexidecimal value
 *
 * Parameters:
 * 	hex_char - a character from the hex string
 *
 * Returns:
 * 	a boolean response as to whether the value is valid or not
 */

bool check_hex_char(char hex_char) {
	// Checks if the char is 0-9, then checks if the char
	// is A-F, then a-f.
	if ((int) hex_char > 47 && (int) hex_char < 58) {
		return true;
	} else if (((int) hex_char > 64 && (int) hex_char < 71) || 
			((int) hex_char > 96 && (int) hex_char < 103)) {
		return true;
	}	
	
	return false;
}

/*
 * Checks if the hexidecimal has valid characters
 * 
 * Parameters:
 * 	hex - the hexidecimal character array we want to check
 * 	hex_len - the size of the passed in character string
 *
 * Returns:
 * 	a boolean with the result of the check, true if valid, false if nonvalid
 */
bool check_hex(const char* hex, size_t hex_len) {
	// Loop through and call check_hex_char to check every character in the string
	for (size_t i = 0; i < hex_len; i++) {
		if (!check_hex_char(hex[i])) {
			return false;
		}

	}
	
	return true;
}

/*
 * bits_in_hex is used to determine how many bits are being used by the 
 * most significant set of bits in a hexidecimal character's integer representation
 *
 * Parameters:
 * 	most_sig - the most significant hexidecimal character 
 *
 * Return:
 * 	an integer representing the number of bits the most_sig character uses
 * 	
*/
int bits_in_hex(char most_sig) {
	int test = 0;
	// Maybe change this to exclude 0 and treat
	// it as a incorrect hexidecimal entry
	//
	if ((int) most_sig < 56 && (int) most_sig > 47) {
		test = (int) most_sig - 48;
		if ((int) most_sig == 48) {
			return 0;
		} else if (test == 1) {
			return 1;
		} else if (test > 1 && test < 4) {
			return 2;
		} else if (test >= 4) {
			return 3;
		}
	}
	// If all other conditions are past, then all 4 bits are used
		return 4;
}

/*
 * Count bits hex takes a hexidecimal and determines the number of bits the hexidecimal uses. 
 * 
 * Parameters:
 * 	hex - a constant char array that contains the desired hexidecimal
 * 
 * Returns:
 * 	a size_t value that represents the number of bits that would be used by the hexidecimal
 */
size_t count_bits_hex(const char *hex) {
	// Multiply the length of the hex by 4 since every character represents 4 bits
	size_t bit_number = get_hex_length(hex) * 4;
	// Get the bits occupied by the most significant character (since it could be <4)
	size_t signif_val = bits_in_hex(hex[0]);
	
	// Remove the last 4 bits and replace with the number of bits occupied by the 
	// most significant character
	bit_number = bit_number - 4 + signif_val;
	return bit_number;
}

/*
 * Creates an ApInt representation of whatever hex value is passed in
 *
 * Parameters:
 * 	hex - const char array that has the hexidecimal we want to convert
 *
 * Returns:
 * 	ApInt pointer of the passed in hex
 */
ApInt *apint_create_from_hex(const char *hex) {
	// Get size of hex, then check if the hexidecimal is valid
	size_t hex_length = get_hex_length(hex);
	if (!check_hex(hex, hex_length)) {
		return NULL;
	}
	
	// Calculate the number of bits being used by the hex, divide by 64,
	// and take the ceiling of the result to find the proper number of 
	// indices for the ApInt string
	size_t num_bits = count_bits_hex(hex);
	size_t size_array = (size_t) ceil((double) num_bits /(double) 64);
	
	// Allocate space for the ApInt output
	uint64_t *new_Ap_num = (uint64_t*) malloc(sizeof(uint64_t) * size_array);
	ApInt *Ap_hex = (ApInt*) malloc(sizeof(ApInt));

	// Cycle through the hex. Everytime you get through 16 hex, it is equal to 
	// 64 bits in uint64_t and so that number will be the first slot. Which means 
	// every 16 hex characters will fill one index in the ApInt array
	uint64_t part_integer = 0;
	uint64_t char_as_int = 0;
	size_t rolling_index = 0;
	int internal_count = 0;
	for (size_t i = 0; i < hex_length; i++) {
		// Convert hex char to its int equivalent ('0' = 0, ..., 'f' = 15)
		char_as_int = hex_char_to_int((int) hex[(hex_length - 1) - i]);
		// Calculate the actual hex char's value by multiplying it by 16 to the position
		// of the character in the array then add it to a rolling sum. But since we are 
		// storing every 16 char in each index, the exponent is mod 16 to get correct 
		// values <= uint64_t MAX
		part_integer += char_as_int * (uint64_t) pow(16, i % 16);
		internal_count++;

		// If the internal count is 16 then dump the rolling sum into the
		// lowest unfilled ApInt index. Then reset the rolling sum to 0. If
		// we have hit the end of the hex internal_count is not divisble by
		// 16, then still dump the rolling sum into the last index of ApInt  
		if (internal_count % 16 == 0 && internal_count != 0) {
			new_Ap_num[rolling_index] = part_integer;
		      	rolling_index++;
			part_integer = 0;
		} else if (rolling_index == size_array - 1 && i == hex_length - 1) {
			new_Ap_num[rolling_index] = part_integer;
		}
	}

	// Build and output Ap_hex
	Ap_hex->Ap_size = check_leading_zeroes(new_Ap_num, size_array);
	Ap_hex->Ap_num_array = new_Ap_num;

	return Ap_hex;
}

/*
 * Frees an allocated ApInt struct 
 *
 * Parameters:
 * 	ap - an ApInt we want to deallocate
 *
 * Returns:
 *	void
 */
void apint_destroy(ApInt *ap) {
	free(ap->Ap_num_array);
	free(ap);
}

/*
 * Gets the "bits" (which is represented by a uint64_t) in the specified index of the ApInt
 *
 * Parameters:
 * 	ap - the ApInt we are extracting the integer from
 * 	n - an unisgned value that is the index of ApInt we want to access
 *
 * Returns:
 * 	the uint64_t found at index n in ap
 */

uint64_t apint_get_bits(ApInt *ap, unsigned n) {
	// If n is larger than the size of the ApInt, return 0
	if (n >= ap->Ap_size) {
		return 0UL;
	}
	
	return ap->Ap_num_array[n];
}

/*
 * Gets the highest significant bit from an ApInt
 *
 * Parameters:
 * 	ap - the ApInt we want the highest bit from
 *
 * Returns:
 * 	an integer representing the bit position of the highest bit
 */
int apint_highest_bit_set(ApInt *ap) {
	size_t size = ap->Ap_size;
	
	// Start at an exponent of 63 and do a bitwise-and comparison with 2 to the power of
	// 63 (exp). If the resulting value is 2 to exponent 63, then return the exponent plus
	// the number of indices in ApInt * 64 for the exact bit in the ApInt number. If the
	// resulting value after the comparison is not 2 to exponent 63, then decrement the
	// exponent and try again
	uint64_t largest_value = ap->Ap_num_array[(int)size - 1];
	for (int exp = 63; exp >= 0; exp--) {
		if (((uint64_t) pow(2, exp) & largest_value) == (uint64_t) pow(2, exp)) {
				return exp + (size - 1) * 64;
		}
	}
	
	// If it reaches here, then the highest index has no on bits
	// so then it must be 0. This should only happen when ApInt is 
	// 1 index long and it is 0;
	return -1;
}

/*
 * Left shifts an ApInt number by 1
 *
 * Parameters:
 * 	ap - the ApInt value we want to left shift
 * Returns:
 * 	the left shifted ApInt pointer
 */

ApInt *apint_lshift(ApInt *ap) {
	size_t size = ap->Ap_size;
	ApInt *Ap_shifted = (ApInt*) malloc(sizeof(ApInt));
	uint64_t *shifted_array = (uint64_t*) malloc(sizeof(uint64_t) * size);
	
	uint64_t carry = 0;
	// Cycle through array and place left shifted number in new
	// Apint struct. Check if number has leading 1 and carry it
	for (size_t i = 0; i < ap->Ap_size; i++) {
		shifted_array[i] = ap->Ap_num_array[i] << 1;

		// Checks if the current index has a leading 1 and if its the last index,
		// then carries it over to the next index's value reallocs space for this extra
		// bit and then adds the bit into the new index. Then check if the index
		// just has a leading bit and ensures it is carried over by incrementing
		// carry. Then if checks if carry has been implemented and adds 1 to the 
		// current index.
		if (i == size - 1 && ((ap->Ap_num_array[i] & (uint64_t) pow(2,63)) != 0)) {
			size++;
			shifted_array =(uint64_t*) realloc(shifted_array, sizeof(uint64_t) * size);
			shifted_array[i+1] = 1;	
		} else if ((ap->Ap_num_array[i] & (uint64_t) pow(2, 63)) != 0) {
			if (carry > 0) {
				shifted_array[i]++;
				carry--;
			}
			carry++;
		} else if (carry > 0) {
			shifted_array[i] += 1;
			carry--;
		}
	}
	Ap_shifted->Ap_num_array = shifted_array;
	Ap_shifted->Ap_size = size;
	return Ap_shifted;
}

/*
 * Allocates and sets up array depending on the value of *move*. If move > 0 then
 * the original n >= 64 so we need to move every index *move* positions up. Then
 * we arrive at a shift less than 64 and can apply it to this array in the 
 * original function. Designed to set up an ApInt that can by shifted by any amount
 * by first standardizing the new ApInt such that any further shifts are by a value
 * < 64
 *
 * Parameters:
 * 	org_ap - the original ApInt before we shift it
 * 	move - a size_t value that indicates by how much each index should shift up
 *
 * Returns:
 * 	An ApInt pointer with its indices shifted if move > 0
 */
ApInt* set_shifted_array(ApInt* org_ap, size_t move) {
	// Use calloc to set all indices to 0
	size_t new_size = org_ap->Ap_size + move;
	uint64_t *new_num = (uint64_t*) calloc(new_size, sizeof(uint64_t));
	
	// If move does not equal 0, then shift the numbers in each index in 
	// the original ApInt *move* number of indices in the output ApInt
	if (move != 0) {
		for (size_t i = 0; i < org_ap->Ap_size; i++) {
			new_num[i+move] = org_ap->Ap_num_array[i];
		}
	}
	
	// Create and output the new ApInt value based on what was created above
	ApInt* ap_new = (ApInt*) malloc(sizeof(ApInt));
	ap_new->Ap_num_array = new_num;
	ap_new->Ap_size = new_size;
	
	return ap_new;
}

/*
 * Function that checks the beginning of an index to see if any bits will be carried over 
 * to the next index. The output is of type uint64_t because this is meant to work only
 * within each index of the ApInt array.
 *
 * Parameters:
 * 	index_num - the uint64_t we are checking for potential overflow bits
 * 	n - an unsigned value that indicates the number of bits we need to check
 * 	for potential overflow (value of the shift in calling function)
 *
 * Returns:
 * 	a uint64_t that contains the bits to be carried over represented as the integer
 * 	said bits represents
 */
uint64_t check_leading_bits(uint64_t index_num, unsigned n) {
	uint64_t highest_bit_on = pow(2, 63);
	uint64_t bit_holder = 0;
	uint64_t carry_sum = 0;

	// Loops until n bits at the front have been checked and all on bits and their
	// respective positions have been added to the bit group being carried 
	for (unsigned i = n; i > 0; i--) {
		bit_holder = 0;
		
		// Checks if the highest bit in the current number is on, if so, it 
		// adds it to the bit group that is being carried to the next index.
		if ((highest_bit_on & index_num) == highest_bit_on) {
			// Places a 1 in position i of bit_holder then adds that value to
			// a rolling sum
			bit_holder |= (1 << i);
		        carry_sum += bit_holder;	
		}
		
		// The highest bit is shifted right so it becomes the second highest bit,
		// then third highest, etc.
		highest_bit_on = highest_bit_on >> 1;
	}

	return carry_sum;

}

/*
 * Function left shifts an ApInt n number of times.
 *
 * Parameters:
 * 	ap - the ApInt we want to shift
 * 	n - the amount we want to shift ap by
 *
 * Returns:
 * 	a n-shifted ApInt struct pointer
 */

ApInt *apint_lshift_n(ApInt *ap, unsigned n) {
	// Instantiate variables and shift each index *move* indices forward. Then find the 
	// resulting < 64 shift that will be done after the index shift. Develop the ApInt
	// now with the built in index shift so that the rest of the function can execute
	// whether n >= 64 or not.
	uint64_t carry_over = 0;
	unsigned move = n / 64;
	unsigned shift = n - (move * 64);
	ApInt* ap_n = set_shifted_array(ap, move);
	
	// Get the size of the original ApInt and use it as the conditional in the for loop
	// This loop goes index by index and performs the left shift  
	size_t size_old = ap->Ap_size;
	for (size_t i = 0; i < size_old; i++) {
		// Shifts the original number over by *shift* bits where shift < 64 
		ap_n->Ap_num_array[i + move] = ap->Ap_num_array[i] << shift;
		
		// Checks if there is a carried value from the previous index that needs to be
		// added to this index and adds it to the current index if there is
		if (carry_over != 0) {
			ap_n->Ap_num_array[i+move] += carry_over;
		}

		// Calls a function to calculate the number of bits and the size_t value of the
		// bits to be added to the following index as carried bits
		carry_over = check_leading_bits(ap->Ap_num_array[i], shift);
		
		// Checks if the last index in the new ApInt has a carry over value, if so, 
		// the array is realloc'd so the carry over value can be added to the ApInt
		if (i == (size_old - 1) && carry_over != 0) {
			ap_n->Ap_size++;
			ap_n->Ap_num_array = (uint64_t*) realloc(ap_n->Ap_num_array, sizeof(uint64_t) * ap_n->Ap_size);
			ap_n->Ap_num_array[ap_n->Ap_size - 1] = carry_over; 
		}
	}
	
	return ap_n;
}

/*
 * Left shifts all the characters in a hex array
 *
 * Parameters:
 * 	full_hex - a char pointer to the hexidimal string we want to shift
 *
 * Returns:
 * 	void
 */
void shift_hex(char *full_hex) {
	for (size_t i = 0; i < strlen(full_hex); i++) {
		full_hex[i] = full_hex[i + 1];
	}
}

/*
 * Appends a set of 16 (or less) characters to a hex string
 *
 * Parameters:
 * 	hex_full - the full hexidemical char pointer that we want to append more hex characters to
 * 	hex_part - a char pointer with a segment of hex characters we want to add to the full hex
 * 	hex_length - size_t value that represents the size of the full hexidecimal
 * 	ind_mult - an integer that represents how many sets of 16 chars are already in the full hexidecimal
 *
 * Returns:
 * 	void
 */
void append_hex(char *hex_full, char*hex_part, size_t hex_length, int ind_mult) {
	for (int i = 0; i < 16; i++) {
		hex_full[((hex_length - 2) - ind_mult * 16) - i] = hex_part[15 - i];
	}
}

/*
 * Converts a 4 bit integer to hex
 *
 * Parmeters:
 * 	integer - a unint64_t (0-15) that we want to convert to a hex character
 *
 * Returns:
 * 	a char that is the hexidecimal representation of the 4 bit integer
 */
char convert_to_hex(uint64_t integer) {
	if (integer < 10) {
		return (char) integer + 48;
	} else {
		// occurs when integer < 16 which is all other cases since
		// a 4 bit int is being passed in as a parameter
		return (char) integer + 87;
	}
}

/*
 * Goes through an ApInt index and analyzes sets of 4 bits until the whole number
 * is converted to hex
 *
 * Parameters:
 * 	hex_trans - the char pointer that has hex values that will be tranformed from int values
 * 	integer - a uint64_t that we want to parse through and convert to correct hex_values
 *
 * Returns:
 * 	void
 */
void parse_ap(char *hex_trans, uint64_t integer){
	for (int i = 0; i < 16; i++) {
		uint64_t lowest_4_bits = integer & ~(~0U << 4);
		hex_trans[15 - i] = convert_to_hex(lowest_4_bits);
		integer = integer >> 4;
	}
}

/*
 * Formats an ApInt to hexidecimal
 *
 * Parameters:
 * 	ap - an ApInt pointer to an ApInt we want to format as hexidecimal
 *
 * Returns:
 * 	char pointer to the hexidecimal representation of ap
 */
char *apint_format_as_hex(ApInt *ap) {
	// check if the number represented by ap is 0, if so, return
	// a simple hex with 0
	if (ap->Ap_size == 1 && ap->Ap_num_array[0] == 0) {
		char * hex_full = (char*) malloc(sizeof(char) * 2);
		hex_full[0] = '0';
		hex_full[1] = '\0';
		return hex_full;
	}
	
	// Allocate memory for a hex string buffer and the entire hex we will output
	size_t hex_length = ((ap->Ap_size * 16) + 1);
	char *hex_full = (char*) calloc(hex_length, sizeof(char));
	char *hex_part = (char*) calloc(17, sizeof(char));
	hex_full[hex_length - 1] = '\0';
	hex_part[16] = '\0';
	uint64_t ap_part = 0;

	// Loop through each index in ap and store its value in ap_part
	// Then fill hex_part with the hexidecimal representation of a_part
	// Finally append hex_part to the end of hex_full
	for (size_t i = 0; i < ap->Ap_size; i++) {
		// If accessing indexed values gets weird, check this out: 
		ap_part = ap->Ap_num_array[i];
		parse_ap(hex_part, ap_part);
		append_hex(hex_full, hex_part, hex_length, i);
	}
	
	// Check if there are any leading zeroes, if so left shift the hex_full string
	// to get rid of them.
	for (size_t i = 0; i < 16; i++) {
		if (hex_full[0] == '0') {
			shift_hex(hex_full);
		}
	}

	free(hex_part);

	return hex_full;
}

/*
 * Adds two ApInt values together
 *
 * Parameters:
 * 	a - const pointer to ApInt
 * 	b - const pointer to ApInt
 *
 * Returns:
 * 	an ApInt pointer with the sum of a and b
 */
ApInt *apint_add(const ApInt *a, const ApInt *b) {
	const ApInt* lesser_ap;
	const ApInt* greater_ap;
	// find and store which of the two values is the larger value
	if (apint_compare(a, b) == 1) {
		lesser_ap = b;
		greater_ap = a;
	} else if (apint_compare(a,b) == -1) {
		lesser_ap = a;
		greater_ap = b;
	} else {
		lesser_ap = a;
		greater_ap = b;
	}
	
	// Allocate the output ApInt with size of the larger ApInt parameter
	ApInt *sum = (ApInt*) malloc(sizeof(ApInt));
	sum->Ap_size = greater_ap->Ap_size;
	uint64_t *sum_array = (uint64_t*) malloc(sizeof(uint64_t) * sum->Ap_size);
	uint64_t carry_over = 0;
	uint64_t index_sum = 0;

	// Loop through all indices of the larger ApInt parameter
	for (size_t i = 0; i < greater_ap->Ap_size; i++) {
		// Check to ensure that i is in bounds for the smaller ApInt
		if (i < lesser_ap->Ap_size) {
			// Overflow may occur
			index_sum = greater_ap->Ap_num_array[i] + lesser_ap->Ap_num_array[i];
		} else {
			index_sum = greater_ap->Ap_num_array[i];
		}

		if (carry_over != 0) {
			index_sum++;
			carry_over--;
		
		}
		
		// Store the index sum in our output array
		sum_array[i] = index_sum;

		// Check for overflow by seeing if the index sum is less than one of the
		// addition operands
		if (index_sum < greater_ap->Ap_num_array[i]) {
			carry_over++;
		}
		
		// Check if this is the last iteration of the loop and there is a 
		// value to be carried over. If so we reallocate one more index and 
		// place the carry_over value in said index
		if (i == greater_ap->Ap_size - 1 && carry_over != 0) {
			sum->Ap_size++;
			sum_array = (uint64_t*) realloc(sum_array, sizeof(uint64_t) * sum->Ap_size);
			sum_array[i + 1] = carry_over;
		}	
	}

	sum->Ap_num_array = sum_array;
	return sum;
}

/*
 * Checks if the ApInt has leading zeros and reallocates for a tighter ApInt
 * representation
 *
 * Parameters:
 * 	diff_array - a unint64_t pointer with the final difference array from apint_sub
 * 	diff_size - a size_t value that is the size of the diff_array
 *
 * Returns:
 * 	a size_t value that is the new size of the realloc'd diff_array (or the same as
 * 	diff_size if no realloc)
 */
size_t check_leading_zeroes(uint64_t *diff_array, size_t diff_size) {
	size_t shrink_by = 0;
	// Loop through diff_array starting at the end and for every iteration where 
	// the index holds 0, increment shrink_b. break from the loop as soon as an 
	// index is reached that is not just 0
	for (size_t i = diff_size; i > 0 ; i--) {
		if (diff_array[i - 1] == 0) {
			shrink_by++;
		} else {
			break;
		}
	}
	
	// Calculate the new size out and check if it has changed, if so then realloc
	// the diff_array to make it smaller and return the new size. Otherwise just return
	// the original size of diff_array
	size_t size_out = diff_size - shrink_by;
	if (size_out != diff_size && size_out != 0) {
		diff_array = (uint64_t*) realloc(diff_array, sizeof(uint64_t) * size_out);
	} else if (size_out == 0) {
		return diff_size;
	}

	return size_out;
}

/* 
 * Subtracts two ApInts, a - b. If b > a, return NULL
 *
 * Parameters:
 * 	a - ApInt pointer that will be subtracted from
 * 	b - ApInt pointer that we will subtract from a
 *
 * Returns:
 * 	ApInt pointer to the difference between a and b
 */ 
ApInt *apint_sub(const ApInt *a, const ApInt *b) {
	// If b > a, return NULL
	if (apint_compare(a, b) == -1) {
		return NULL;
	}

	// Allocate the output array using the size of the larger ApInt as the size
	// of the output ApInt 
	size_t diff_size = a->Ap_size;
	ApInt *diff = (ApInt*) malloc(sizeof(ApInt));
	uint64_t *diff_array = (uint64_t*) malloc(sizeof(uint64_t) * diff_size);
	
	// Iterate through all indices of a (and our output)
	for (size_t i = 0; i < diff_size; i++) {
		// If i is larger than the size of b, slot in a's ith number into our 
		// output's ith place. If a < b, decrement the next index of a to simulate
		// "borrowing" and simply subtract letting overflow handle the result. 
		// Otherwise, simply subtract the two integers
		if (i >= b->Ap_size) {
			// Assumes that the any borrowing effect is already applied
			// to a's number representation
			diff_array[i] = a->Ap_num_array[i];
		} else if (a->Ap_num_array[i] < b->Ap_num_array[i]) {
			a->Ap_num_array[i + 1]--;
			diff_array[i] = a->Ap_num_array[i] - b->Ap_num_array[i];
		} else {
			diff_array[i] = a->Ap_num_array[i] - b->Ap_num_array[i];
		}
	}
	
	// Check for leading zeroes and reallocate an appropriate sized array to get rid
	// of leading zeroes
       	diff->Ap_size = check_leading_zeroes(diff_array, diff_size);
	diff->Ap_num_array = diff_array;
	return diff;
}

/*
 * Compares two ApInts by first comparing their sizes, then comparing the
 * values in each index of their array. After checking every index's value, 
 * if all values are equal, 0 is returned.
 *
 * Parameters:
 * 	left - ApInt pointer
 * 	right - ApInt pointer
 *
 * Returns:
 * 	an integer describing the comparison where 1 means left > right, -1 if left < right,
 * 	and 0 if left == right
 */

int apint_compare(const ApInt *left, const ApInt *right) {
	// If size of left ApInt is larger, return 1, if size of right ApInt
	// is larger, return -1. If the sizes are equal, then loop through 
	// each ApInt array and compare the numbers in each index returning
	// the same results as above if found. If nothing is found, the loop
	// exits and the if statement exits, so 0 is returned and they are equal.
	if (left->Ap_size > right->Ap_size) {
		return 1;
	} else if (left->Ap_size < right->Ap_size) {
		return -1;
	} else {
	        // Loop through the entirety of both ApInt parameters comparing the
		// integers held in each index. As soon as a meaningful result is found,
		// simply return it	
		size_t ap_sizes = left->Ap_size;
		for (size_t i = ap_sizes; i > 0; i--) {
			if (left->Ap_num_array[i - 1] > right->Ap_num_array[i - 1]) {
				return 1;	
			} else if (left->Ap_num_array[i - 1] < right->Ap_num_array[i - 1]) {
				return -1;
			}
		}
	}
	
	// If the function has reached this point, then both ApInt parameters were fully 
	// looped through and compared with no difference being found between them
	return 0;
}

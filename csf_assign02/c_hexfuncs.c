// C implementation of hexdump functions
// Andres Recalde
// arecald1@jhu.edu

#include <unistd.h>  // this is the only system header file you may include!
#include "hexfuncs.h"

/* 
 * Read up to 16 bytes from standard input into data_buf and
 * return characters read
 *
 * Parameters:
 * 	data_buf -  a character string
 *
 * Returns:
 *	a long value describing the number of characters read
 */
long hex_read(char data_buf[]) {
  int read_amount = 0;
  char one_char[2];
  // Read one character at a time until the array is full of
  // 16 characters or there is nothing left to read
  while (read_amount < 16 && read(0, one_char, 1)) {
    data_buf[read_amount] = one_char[0];
    read_amount++;
  }
  
  // Return number of characters read
  return read_amount;
}

/* 
 * Returns length of a string and should not include null terminator
 * in the calculation.
 *
 * Parameters:
 * 	s - a constant character string
 *
 * Returns:
 * 	an integer representing the length of the string excluding '\0'
 */
int len_str(const char s[]) {
  int length = 0;
  char holder = s[0];
  while (holder != '\0') {
  length++;
  holder = s[length];
  }

  return length;
}

/*
 * Write given null terminated string to standard output
 *
 * Parameters:
 * 	s - constant character string to be printed
 *
 * Returns:
 * 	nothing
 */
void hex_write_string(const char s[]) {
  int avoid_warning = write(1, s, len_str(s));
  (void) avoid_warning;
}

/*
 * Helper function that takes in an integer and converts it to a hex character
 *
 * Parameters:
 * 	num - an integer value to be converted to hex char
 *
 * Returns:
 * 	a character representation of the passed in integer
 */
char format_as_hex_char(int num) {
  if (num >= 0 && num <= 9) {
    return (char) num + 48;
  } else {
    // Should reach here if 9 < num < 16 
    return (char) num + 87;
  }
}

/*
 * Formats a long integer that signifies the offset into a string of 8 hex 
 * digits.
 *
 * Parameters:
 * 	offset - the 32-bit integer that represents the string offset
 * 	sbuf - a char string that must have length of at least 8 and will
 * 		store the result of the integer to hexidecimal conversion
 *
 * Returns:
 * 	nothing
 */
void hex_format_offset(long offset, char sbuf[]) {
  int four_bit_int = 0;
  char new_hex_char = '.';
  
  // Loop 8 times to convert each set of least significant digits to a hex char
  for(int i = 7; i >= 0; i--) {
    // Get the least significant 4 bits of offset and store it
    // Right shift offset by 4, then convert the previously 
    // acquired 4 bit integer and store it in the correct index	  
    four_bit_int = (offset & ~(~0U << 4));
    offset = offset >> 4;
    new_hex_char = format_as_hex_char(four_bit_int);
    sbuf[i] = new_hex_char;
  }

  // Append a null character
  sbuf[8] = '\0';
}

/*
 * Formats a byte value (in range 0-255) as a string of 2 hex digits
 *
 * Parameters:
 * 	byteval - a long value that stores the vale we want converted to hex
 * 	sbuf - a string of length 3 that will store the result of the 
 * 	conversion with a null terminator
 *
 * Returns:
 * 	nothing
 */
void hex_format_byte_as_hex(long byteval, char sbuf[]) {
  // Get the least significant 4 bits of byteval and store them
  int first_four_bits = (byteval & ~(~0U << 4));
  
  // Left shift byteval by 4 to get the most significant 4 bits
  byteval = byteval >> 4;
 
  // Convert both integers acquired into hex characters and store them
  // in the correct indices
  sbuf[1] = format_as_hex_char(first_four_bits);
  sbuf[0] = format_as_hex_char(byteval);
  sbuf[2] = '\0';
}

/*
 * Checks if a long integer value is a printable char value, if it is,
 * then simply return the long integer value. If it is not, return the 
 * char value for '.'
 *
 * Parameters:
 * 	byteval - a long value that may or may not represent a printable char
 *
 * Return:
 * 	a long value representing the char we want to display
 */
long hex_to_printable(long byteval) {
  // If byteval if less than 33 or greater than 126 it is unprintable
  if (byteval <= 31 || byteval >= 127) {
    return 46;
  } else {
    return byteval;
  }
}

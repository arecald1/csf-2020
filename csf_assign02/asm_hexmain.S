/* Hexdump main function, assembly language version */
/* Andres Recalde arecald1@jhu.edu */
	.section .text

	.section .data
offset_S:	.zero 9
input:		.zero 17
hex_byte:	.zero 3
output:		.zero 77

	.globl main
	.globl offset
	.globl check_for_converted_bytes

/* 
 * Helper function to create and store offset string. %rdi and %rsi are
 * immediately passed into the hex_format_offset function
 *
 * Parameters:
 * 	%rdi - current value of offset
 *	%rsi - stores the string conversion of the offset value
 *	%rdx - holds the output string of the entire main function
 *
 * Return:
 * 	nothing
 */
offset:
	subq $8, %rsp 			/* align stack pointer */
	
	call hex_format_offset		/* uses passed in argument registers %rdi and %rsi */
	
	movq $0, %r10			/* put 10 in our counter register for for loop */

.LoffsetLoop:
	movb (%rsi, %r10), %r11b	/* move the char in index %r10 of %rsi into %r11 */
	movb %r11b, (%rdx, %r10) 	/* move char in %r11 into index %r10 of %rsi */
	incq %r10			/* increment our counter register */  

.LoffsetCheck:
	cmpq $8, %r10			/* compare 8 to our counter register %r10 */
	jl .LoffsetLoop			/* if %r10 is less than 8, continue the loop */

	movb $58, 8(%rdx)		/* move ACSII colon value in index 8 of offset string */
	movb $32, 9(%rdx)		/* move ACSII space value in index 9 of offset string */
	
	addq $8, %rsp			/* restore stack pointer */
	ret
/*
 * Helper function that checks if there are converted hex bytes to
 * print and if there isn't, stores a space char in output string.
 * 
 * Parameters:
 * 	%rdi - output string
 *	%rsi - hex_byte
 *	%rdx - bytes read
 *	%rcx - loop counter - will be referred to as i in comments
 *
 * Returns:
 *	nothing
 */

check_for_converted_bytes:
	subq $8, %rsp			/* align stack pointer */
	
	movq $3, %r11			/* move 3 into temp variable register */
	imulq %rcx, %r11		/* store 3*i in temp variable register */
		
	cmpq %rdx, %rcx 		/* compare bytes read to i */
	jge .LcheckElse			/* if i is greater than or equal to bytes read, jump to else */
	
	movb (%rsi), %r10b		/* move the first char in hex_byte to temp variable register */
	movb %r10b, 10(%rdi, %r11)	/* move first char in hex_byte to index 10 + i*3 of output */
	
	movb 1(%rsi), %r10b		/* move the second char in hex_byte to temp variable register */
	movb %r10b, 11(%rdi, %r11)	/* move the character to index 11 + i*3 of output */
	jmp .LcheckDone

.LcheckElse:
	movb $32, 10(%rdi, %r11)	/* move ASCII space value to index 10 + i*3 of output */
	movb $32, 11(%rdi, %r11)	/* move ASCII space value to index 11 + i*3 of output */

.LcheckDone:	
	movb $32, 12(%rdi, %r11)	/* move a ASCII space into index 12 + i*3 of output string */

	addq $8, %rsp			/* restore stack pointer */
	ret
	
main:
	pushq %rbx			/* store value in %rbx */
	pushq %r12			/* store value in %r12 */
	pushq %r13			/* store value in %r13 */		
	
	movq $16, %rbx			/* "initialize" our bytes read register with 16 */
	movq $0, %r12			/* "initialize" our offset tracking register with 0 */
	
.LmainLoopAndCheck:
	cmpq $16, %rbx			/* compare 16 and our bytes read register */
	jl .LmainDone			/* if bytes read is less than 16, end loop */
	
	movq $input, %rdi		/* move the input global variable into first argument */
	call hex_read			/* call read and read into input string */
	movq %rax, %rbx			/* move the returned bytes read to bytes read register */
	
	cmpq $0, %rbx			/* compare 0 and the number of bytes read */
	je .LmainDone			/* if bytes read and 0 are equal, end loop */

	movq %r12, %rdi			/* move the offset tracker into the first argument register */
	movq $offset_S, %rsi		/* move the offset string into second argument register */
	movq $output, %rdx		/* move output string into third argument register */
	
	call offset			/* call helper function to create and store offset string */ 
	addq $16, %r12			/* add 16 to the offset tracker */
	
	movq $0, %r13			/* "initialize" our loop counter register with 0 */
.LmainByteLoop:	
	movq $hex_byte, %rsi		/* move our hex_byte buffer string to second argument */
	movq $input, %r10		/* move our input string into temporary variable */
	
	movb (%r10, %r13), %dil 	/* move char at index %r13 of input to first argument */
	
	call hex_format_byte_as_hex 	/* fill hex_byte with the hex chars converted from input char */
	
	movq $input, %r10		/* move input string into temporary variable register */
	movb (%r10, %r13), %dil		/* store the character at index %r13 of input to first argument */
	
	call hex_to_printable		/* get the character we are printing to standard out */

	movq $output, %rdi		/* move output string into first argument register */
	movb %al, 59(%rdi, %r13)	/* move char returned from call into output string at 
					index %r13 + 59 */
	
	movq $hex_byte, %rsi		/* move hex_byte into second argument register */
	movq %rbx, %rdx			/* move bytes read tracker into third argument register */
	movq %r13, %rcx			/* move loop counter/index into fourth argument register */
	call check_for_converted_bytes	/* stores the correct char value in output string based on
					a comparison of the counter and bytes read */
	
	incq %r13			/* increment for loop counter */

.LmainByteLoopCheck:
	cmpq $16, %r13			/* compare 16 and our loop counter */
	jl .LmainByteLoop		/* if the loop counter is less than 16, continue looping */
	
	movq $output, %rdi		/* move our output string to first argument */
	movb $32, 58(%rdi)		/* move 32 into index 58 of output string */
	movq $16, %r10			/* move 16 into temporary variable register */
	subq %rbx, %r10			/* subtract bytes read from 16 */
	movq $75, %r11			/* move 75 into another temporary variable register */
	subq %r10, %r11			/* subtract the previous difference from 75 */

	movb $10, (%rdi, %r11)  	/* move ASCII newline value into index 75 - (16 - bytes read) 
					of output string */
	incq %r11			/* increment the index */
	movb $0, (%rdi, %r11)		/* place ASCII null-terminator at index 76 - (16 - bytes read)
					of output string */
	call hex_write_string		/* print output string to standard output */
	
	jmp .LmainLoopAndCheck 
	
.LmainDone:
	movq $0, %rax			/* return 0 from main */
	
	popq %r13			/* retore value in %r13 */
	popq %r12			/* restore value in %r12 */
	popq %rbx			/* restore value in %rbx */
	ret

/* vim:ft=gas:
 */

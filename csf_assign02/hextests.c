// Unit tests for hex functions
// These tests should work for both your C implementations and your
// assembly language implementations
// Andres Recalde arecald1@jhu.edu

#include <stdio.h>
#include <stdlib.h>
#include "tctest.h"
#include "hexfuncs.h"

// test fixture object
typedef struct {
  char test_data_1[16];
  char alphanumeric[40];
} TestObjs;

// setup function (to create the test fixture)
TestObjs *setup(void) {
  TestObjs *objs = malloc(sizeof(TestObjs));
  strcpy(objs->test_data_1, "Hello, world!\n");
  strcpy(objs->alphanumeric, "abcdefghijklmnopqrstuvwxyz0123456789\n");
  return objs;
}

// cleanup function (to destroy the test fixture)
void cleanup(TestObjs *objs) {
  free(objs);
}

// Prototypes for test functions
void testFormatOffset(TestObjs *objs);
void testFormatByteAsHex(TestObjs *objs);
void testHexToPrintable(TestObjs *objs);

int main(int argc, char **argv) {
  if (argc > 1) {
    tctest_testname_to_execute = argv[1];
  }

  TEST_INIT();

  TEST(testFormatOffset);
  TEST(testFormatByteAsHex);
  TEST(testHexToPrintable);

  TEST_FINI();

  return 0;
}

void testFormatOffset(TestObjs *objs) {
  (void) objs; // suppress warning about unused parameter
  char buf[16];
  hex_format_offset(1L, buf);
  ASSERT(0 == strcmp(buf, "00000001"));
  
  // Test the conversion of a maximum sized long
  hex_format_offset(0xffffffffL, buf);
  ASSERT(0 == strcmp(buf, "ffffffff"));
  
  // Test the conversion for 0L
  hex_format_offset(0L, buf);
  ASSERT(0 == strcmp(buf, "00000000"));

  // Stress testing for random numbers
  hex_format_offset(0x894fcaf0L, buf);
  ASSERT(0 == strcmp(buf, "894fcaf0"));

  hex_format_offset(0x0046913fL, buf);
  ASSERT(0 == strcmp(buf, "0046913f"));

  hex_format_offset(0x00000010L, buf);
  ASSERT(0 == strcmp(buf, "00000010"));

  hex_format_offset(0xabcdef12L, buf);
  ASSERT(0 == strcmp(buf, "abcdef12"));
}

void testFormatByteAsHex(TestObjs *objs) {
  char buf[16];
  hex_format_byte_as_hex(objs->test_data_1[0], buf);
  ASSERT(0 == strcmp(buf, "48"));
  
  /* 
   * Test the output of every bit on in a long independently
   * also testing ability to take a long value since that is the
   * correct parameter for the function. Therefore, some output
   * may be redundant ("08" now vs. "08" later)
   */
  hex_format_byte_as_hex(0L, buf);
  ASSERT(0 == strcmp(buf, "00"));
  
  hex_format_byte_as_hex(1L, buf);
  ASSERT(0 == strcmp(buf, "01"));
  
  hex_format_byte_as_hex(2L, buf);
  ASSERT(0 == strcmp(buf, "02"));
  
  hex_format_byte_as_hex(4L, buf);
  ASSERT(0 == strcmp(buf, "04"));
  
  hex_format_byte_as_hex(8L, buf);
  ASSERT(0 == strcmp(buf, "08"));
  
  hex_format_byte_as_hex(16L, buf);
  ASSERT(0 == strcmp(buf, "10"));
  
  hex_format_byte_as_hex(32L, buf);
  ASSERT(0 == strcmp(buf, "20"));
  
  hex_format_byte_as_hex(64L, buf);
  ASSERT(0 == strcmp(buf, "40"));
  
  hex_format_byte_as_hex(128L, buf);
  ASSERT(0 == strcmp(buf, "80"));

  // Test for all bytes on
  hex_format_byte_as_hex(255L, buf);
  ASSERT(0 == strcmp(buf, "ff"));

  // Stress Test aplhanumeric characters
  hex_format_byte_as_hex(objs->alphanumeric[0], buf);
  ASSERT(0 == strcmp(buf, "61"));

  hex_format_byte_as_hex(objs->alphanumeric[1], buf);
  ASSERT(0 == strcmp(buf, "62"));

  hex_format_byte_as_hex(objs->alphanumeric[2], buf);
  ASSERT(0 == strcmp(buf, "63"));

  hex_format_byte_as_hex(objs->alphanumeric[3], buf);
  ASSERT(0 == strcmp(buf, "64"));

  hex_format_byte_as_hex(objs->alphanumeric[4], buf);
  ASSERT(0 == strcmp(buf, "65"));

  hex_format_byte_as_hex(objs->alphanumeric[5], buf);
  ASSERT(0 == strcmp(buf, "66"));

  hex_format_byte_as_hex(objs->alphanumeric[6], buf);
  ASSERT(0 == strcmp(buf, "67"));

  hex_format_byte_as_hex(objs->alphanumeric[7], buf);
  ASSERT(0 == strcmp(buf, "68"));

  hex_format_byte_as_hex(objs->alphanumeric[8], buf);
  ASSERT(0 == strcmp(buf, "69"));

  hex_format_byte_as_hex(objs->alphanumeric[9], buf);
  ASSERT(0 == strcmp(buf, "6a"));

  hex_format_byte_as_hex(objs->alphanumeric[10], buf);
  ASSERT(0 == strcmp(buf, "6b"));

  hex_format_byte_as_hex(objs->alphanumeric[11], buf);
  ASSERT(0 == strcmp(buf, "6c"));

  hex_format_byte_as_hex(objs->alphanumeric[12], buf);
  ASSERT(0 == strcmp(buf, "6d"));

  hex_format_byte_as_hex(objs->alphanumeric[13], buf);
  ASSERT(0 == strcmp(buf, "6e"));

  hex_format_byte_as_hex(objs->alphanumeric[14], buf);
  ASSERT(0 == strcmp(buf, "6f"));

  hex_format_byte_as_hex(objs->alphanumeric[15], buf);
  ASSERT(0 == strcmp(buf, "70"));

  hex_format_byte_as_hex(objs->alphanumeric[16], buf);
  ASSERT(0 == strcmp(buf, "71"));

  hex_format_byte_as_hex(objs->alphanumeric[17], buf);
  ASSERT(0 == strcmp(buf, "72"));

  hex_format_byte_as_hex(objs->alphanumeric[18], buf);
  ASSERT(0 == strcmp(buf, "73"));

  hex_format_byte_as_hex(objs->alphanumeric[19], buf);
  ASSERT(0 == strcmp(buf, "74"));

  hex_format_byte_as_hex(objs->alphanumeric[20], buf);
  ASSERT(0 == strcmp(buf, "75"));

  hex_format_byte_as_hex(objs->alphanumeric[21], buf);
  ASSERT(0 == strcmp(buf, "76"));

  hex_format_byte_as_hex(objs->alphanumeric[22], buf);
  ASSERT(0 == strcmp(buf, "77"));

  hex_format_byte_as_hex(objs->alphanumeric[23], buf);
  ASSERT(0 == strcmp(buf, "78"));

  hex_format_byte_as_hex(objs->alphanumeric[24], buf);
  ASSERT(0 == strcmp(buf, "79"));

  hex_format_byte_as_hex(objs->alphanumeric[25], buf);
  ASSERT(0 == strcmp(buf, "7a"));

  hex_format_byte_as_hex(objs->alphanumeric[26], buf);
  ASSERT(0 == strcmp(buf, "30"));

  hex_format_byte_as_hex(objs->alphanumeric[27], buf);
  ASSERT(0 == strcmp(buf, "31"));

  hex_format_byte_as_hex(objs->alphanumeric[28], buf);
  ASSERT(0 == strcmp(buf, "32"));

  hex_format_byte_as_hex(objs->alphanumeric[29], buf);
  ASSERT(0 == strcmp(buf, "33"));

  hex_format_byte_as_hex(objs->alphanumeric[30], buf);
  ASSERT(0 == strcmp(buf, "34"));

  hex_format_byte_as_hex(objs->alphanumeric[31], buf);
  ASSERT(0 == strcmp(buf, "35"));

  hex_format_byte_as_hex(objs->alphanumeric[32], buf);
  ASSERT(0 == strcmp(buf, "36"));

  hex_format_byte_as_hex(objs->alphanumeric[33], buf);
  ASSERT(0 == strcmp(buf, "37"));

  hex_format_byte_as_hex(objs->alphanumeric[34], buf);
  ASSERT(0 == strcmp(buf, "38"));

  hex_format_byte_as_hex(objs->alphanumeric[35], buf);
  ASSERT(0 == strcmp(buf, "39"));

  // Other characters
  hex_format_byte_as_hex('?', buf);
  ASSERT(0 == strcmp(buf, "3f"));

  hex_format_byte_as_hex('$', buf);
  ASSERT(0 == strcmp(buf, "24"));

  hex_format_byte_as_hex('Q', buf);
  ASSERT(0 == strcmp(buf, "51"));

  hex_format_byte_as_hex('~', buf);
  ASSERT(0 == strcmp(buf, "7e"));
}

void testHexToPrintable(TestObjs *objs) {
  ASSERT('H' == hex_to_printable(objs->test_data_1[0]));
  ASSERT('.' == hex_to_printable(objs->test_data_1[13]));
  
  // Tests every possible acceptable character as input
  for (long i = 0; i < 256L; i++) {
    if (i < 32L || i > 126L) {
      ASSERT('.' == (unsigned char) hex_to_printable(i));
    } else {
      ASSERT(((unsigned char) i) == (unsigned char) hex_to_printable(i));
    }
  }
}

// C implementation of hexdump main function
// Andres Recalde
// arecald1@jhu.edu

#include "hexfuncs.h"  // this is the only header file which may be included!

/*
 * Helper function that checks if the counter is greater than or equal to
 * the number of bytes read. If it is, then store a space in the correct 
 * index. The chars are filled in sets of three with the first indices being
 * 10, 11, and 12 with 12 always being the space between byte strings (filled
 * outside of function).
 *
 * Parameters:
 * 	output - the output string that we will be printing to standard out
 * 	hex_byte - string with 2 byte hex string to be inserted into output
 * 	i - counter/index from the loop in main
 * 	bytes_read - number of bytes read for this loop
 *
 * Return:
 * 	nothing
 *
 */
void check_bytes_read(char output[], char hex_byte[], int i, long bytes_read) {
  if (i < bytes_read) {
    output[10 + i*3] = hex_byte[0];
    output[11 + i*3] = hex_byte[1];
  } else {
    output[10 + i*3] = (unsigned char) 32;
    output[11 + i*3] = (unsigned char) 32;
  }
}

int main(void) {
  long bytes_read = 16;
  int offset = 0;
  char offset_string[9], input[17], hex_byte[3], output[77]; 
   
  while(bytes_read == 16 && (bytes_read = hex_read(input))) { 
    // Set up the offset string and place into beginning of output string  
    hex_format_offset(offset, offset_string);
    offset += 16;
    for (int i = 0; i < 8; i++) {
      output[i] = offset_string[i];
    }
    output[8] = ':';
    output[9] = (char) 32;

    // Loop through the input string and create the converted hex values
    // as well as the third column with the string-like representation 
    for (int i = 0; i < 16; i++) {
      // Find hex representation of a character	    
      hex_format_byte_as_hex((unsigned char) input[i], hex_byte);
      // Store the correct character for the third column string representation
      output[59 + i] = (unsigned char) hex_to_printable(
			(unsigned char) input[i]);
      
      // If the index is greater than bytes_read, fill the rest of the middle
      // column with spaces. Taken care of in helper function below
      check_bytes_read(output, hex_byte, i, bytes_read);
      output[12 + i*3] = (unsigned char) 32; 
    }
    output[58] = (unsigned char) 32;
    // If bytes_read is less than 16, end the third column string early
    output[75 - (16 - bytes_read)] = '\n';
    output[76 - (16 - bytes_read)] = '\0';
    // Output the result to the user
    hex_write_string(output);
  }

  return 0;
}

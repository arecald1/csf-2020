/*
 * Exposire Plugin
 * CSF Assignment 4
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <stdlib.h>
#include "image_plugin.h"

struct Arguments {
	double e_factor;
};

const char *get_plugin_name(void) {
	return "expose";
}

const char *get_plugin_desc(void) {
	return "adjust the intensity of all pixels";
}

void *parse_arguments(int num_args, char *args[]) {
	(void) args; // this is just to avoid a warning about an unused parameter

	if (num_args != 1) {
		return NULL;
	}

	double exp_factor = atof(args[0]);
	
	// atof returns 0.0 if it does not detect a double.
	// This statement detects if the value is negative or invalid
	if (exp_factor <= 0.0) {
		return NULL;
	}
	
	// Allocate a new argument struct and store the valid factor
	// into the argument struct
	struct Arguments* arg = malloc(sizeof(struct Arguments));
	arg->e_factor = exp_factor;

	return arg;
}

// Helper function to apply exposure factor.
static uint32_t expose(uint32_t pix, double e_factor) {
	uint8_t r, g, b, a;
	img_unpack_pixel(pix, &r, &g, &b, &a);
	int color_holder;
	
	// For each color we multiply by the exposure factor, 
	// if the new value is greater than 255, set the output
	// color value to 255
	color_holder = r * e_factor;
	if (color_holder > 255) {
		r = 255;
	} else {
		r = (uint8_t) color_holder;
	}

	color_holder = g * e_factor;
	if (color_holder > 255) {
		g = 255;
	} else {
		g = (uint8_t) color_holder;
	}

	color_holder = b * e_factor;
	if (color_holder > 255) {
		b = 255;
	} else {
		b = (uint8_t) color_holder;
	}

	return img_pack_pixel(r, g, b, a);
}

struct Image *transform_image(struct Image *source, void *arg_data) {
	struct Arguments *args = arg_data;

	// Allocate a result Image
	struct Image *out = img_create(source->width, source->height);
	if (!out) {
		free(args);
		return NULL;
	}

	unsigned num_pixels = source->width * source->height;
	for (unsigned i = 0; i < num_pixels; i++) {
		out->data[i] = expose(source->data[i], args->e_factor);
	}

	free(args);

	return out;
}

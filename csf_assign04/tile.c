/*
 * Tile Plugin
 * CSF Assignment 4
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <stdlib.h>
#include "image_plugin.h"

struct Arguments {
	int t_factor;
};

const char *get_plugin_name(void) {
	return "tile";
}

const char *get_plugin_desc(void) {
	return "tile source image in an NxN arrangement";
}

void *parse_arguments(int num_args, char *args[]) {
	(void) args; // this is just to avoid a warning about an unused parameter

	if (num_args != 1) {
		return NULL;
	}
	
	int tile_factor = atoi(args[0]);

	if (tile_factor <= 0) {
		return NULL;
	}
	
	struct Arguments* arg = malloc(sizeof(struct Arguments));
	arg->t_factor = tile_factor;

	return arg;
}

/*
 * Fills an array with the proper image dimensions
 *
 * Parameter:
 * 	factor - tiling factor
 * 	org_dim - the original dimension we must divide
 *
 * Returns:
 * 	a dynamically allocated array holding the image dimensions
 */
int *get_dimensions(int factor, int org_dim) {
	int *new_dims = malloc(sizeof(int) * factor);
	int new_dim = 0;
	
	// Divides the original dimesion by the factor
	// Stores the result in the array
	// Then subtracts the new value from the original dimension
	// and divides this value by factor - 1 on the next loop
	for (int i = 0; i < factor; i++) {
		new_dim = org_dim / (factor - i);
		new_dims[i] = new_dim;

		org_dim -= new_dim;	
	}

	return new_dims;
}

struct Image *transform_image(struct Image *source, void *arg_data) {
	struct Arguments *args = arg_data;

	// Allocate a result Image
	struct Image *out = img_create(source->width, source->height);
	if (!out) {
		free(args);
		return NULL;
	}
	
	// Store the tile factor into n
	int n = args->t_factor;

	unsigned num_row = source->height;
	unsigned num_col = source->width;
	
	// Get two arrays with the different dimensions for tiles'
	// heights and widths
	// Note: Smaller dimensions are in the front of the array
	int *height_dims = get_dimensions(n, num_row);
	int *width_dims = get_dimensions(n, num_col);
	
	// declare variables for the loops
	int height_sum = 0;
	int width_sum = 0;
	int out_ind = 0;
	int src_ind = 0;
	
	// This loop determines the upper bound of the tile height
	for (int i = n - 1; i >= 0; i--) {

		// This will loop through all the height values of 
		// one tile
		for (int h = 0; h < height_dims[i]; h++) {
			
			// This loop determines the upper bound of the 
			// tile width
			for (int j = n - 1; j >= 0; j--) {
				
				// This will loop through all the width values
				// of one tile
				for (int w = 0; w < width_dims[j]; w++) {
					// Absolute index for output image
					out_ind = ((height_sum + h) * num_col) 
						+ (width_sum + w);

					// Sample indexing from original image
					src_ind = ((h * n) * num_col) + (w * n);
					
					out->data[out_ind] = 
						source->data[src_ind];
							
				}

				// Calculate rolling sum used for offset for
				// absolute indexing
				width_sum += width_dims[j];
			}
			
			// Reset the rolling sum when the width loop terminates
			width_sum = 0;	
		}

		// Calculate rolling sum used for offset for absolute indexing
		height_sum += height_dims[i];
	}
	
	free(height_dims);
	free(width_dims);
	free(args);

	return out;
}

/*
 * Horizontal Mirror Plugin
 * CSF Assignment 4
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <stdlib.h>
#include "image_plugin.h"

struct Arguments {
	// This plugin doesn't accept any command line arguments;
	// just define a single dummy field.
	int dummy;
};

const char *get_plugin_name(void) {
	return "mirrorh";
}

const char *get_plugin_desc(void) {
	return "mirror image horizontally";
}

void *parse_arguments(int num_args, char *args[]) {
	(void) args; // this is just to avoid a warning about an unused parameter

	if (num_args != 0) {
		return NULL;
	}
	return calloc(1, sizeof(struct Arguments));
}

struct Image *transform_image(struct Image *source, void *arg_data) {
	struct Arguments *args = arg_data;

	// Allocate a result Image
	struct Image *out = img_create(source->width, source->height);
	if (!out) {
		free(args);
		return NULL;
	}
	
	// Declare variables for accessing correct data indices
	unsigned num_col = source->width; 
	unsigned num_row = source->height;
	int ind_out = 0;
	int ind_src = 0;

	for (unsigned r = 0; r < num_row; r++) {
		for (unsigned c = 0; c < num_col; c++) {
			// ind_out is the current index
			ind_out = (r * num_col) + c;

			// ind_src is the same row but column component
			// treated the end of the row as the 0 index
			ind_src = (r * num_col) + (num_col - 1 - c);

			// Store the desired pixel in the source into the out
			out->data[ind_out] = source->data[ind_src];
		}
	}


	free(args);

	return out;
}

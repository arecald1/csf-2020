/*
 * The driver for the image processing with library functions
 * CSF Assignment 4
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include "driver_func.h"

int main(int argc, char* argv[]) {
	char * environment_list;
	DIR *plugin_dir;
	struct dirent *entry;

	if (argc == 1) {
		print_usage();
		return 0;
	}
	
	// Check if the environment variable is set
	environment_list = getenv("PLUGIN_DIR");

	// If the environment variable is not set, set the string
	// to the plugins subdirectory
	if (environment_list == NULL) {
		environment_list = "./plugins";
	}
	
	// Open the directory held in environment_list and store the resulting
	// DIR * in pluginDir
	plugin_dir = opendir(environment_list);
	
	// malloc an array of struct plugin pointers. Since each slot is
	// a pointer, we should malloc the size of the pointer times 5
	// for 5 library files that can be realloc'd later if needed
	struct Plugin **plugins_list = malloc(8*5);
	if (!plugins_list) {
		print_error(5);
		closedir(plugin_dir);
		return 5;
	}

	// Loop through pluginDir and check the files. If the filename ends
	// in .so, create a plugin struct from said file and store it in the
	// struct array.
	int num_plug = 0;
	
	// Calls function to loop through all entry pointers to fill
	// the plugins array with compatible plugins	
	entry = readdir(plugin_dir);
	int error = fill_plugins(entry, environment_list, plugins_list, 
			&num_plug, plugin_dir);
	if (error != 0) {
		destroy_list(plugins_list, num_plug);
		closedir(plugin_dir);
		print_error(error);
		return error;
	}

	// read and check input arguments
	error = check_input(argc, argv, plugins_list, num_plug);	
	if (error != 0) {
		destroy_list(plugins_list, num_plug);
		closedir(plugin_dir);
		print_error(error);
		return error;
	}
	
	// Call the function corresponding to the command line request
	if (strcmp(argv[1], "list") == 0) {
		error = list(plugins_list, num_plug);
	} else {
		error = execute(argc, argv, plugins_list, num_plug);
	}

	destroy_list(plugins_list, num_plug);
	closedir(plugin_dir);

	// Check if the execute or list functions throw an error
	if (error != 0) {
		print_error(error);
		return error;
	}

	return 0;
}

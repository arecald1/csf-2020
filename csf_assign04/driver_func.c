/*
 * Functions for the driver main file to use
 * CSF Assignment 4
 * Andres Recalde
 * arecald1@jhu.edu
 */

#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include "image.h"
#include "driver_func.h"

/*
 * Creates a new plugin struct pointer from the specified path to the
 * directory holding the plugin
 *
 * Parameters:
 * 	path - a character string with the file path to the directory
 *
 * Returns:
 * 	a pointer to a plugin struct
 */
struct Plugin *plugin_create(const char *path) {
	struct Plugin *plugin;
	
	// Allocate space for a plugin
	plugin = malloc(sizeof(struct Plugin));
	if (!plugin) {
		return NULL;
	}
	
	// Try to obtain the handle and store it
	plugin->handle = dlopen(path, RTLD_LAZY);
	if (!plugin->handle) {
		free(plugin);
		return NULL;
	}
	

	// Get all the functions into the function pointers
	*(void **) (&plugin->get_plugin_name) = dlsym(plugin->handle, 
			"get_plugin_name");
	*(void **) (&plugin->get_plugin_desc) = dlsym(plugin->handle, 
			"get_plugin_desc");
	*(void**) (&plugin->parse_arguments) = dlsym(plugin->handle, 
			"parse_arguments");
	*(void**) (&plugin->transform_image) = dlsym(plugin->handle, 
			"transform_image");
	
	return plugin;
}

/*
 * Prints the usage message to help a user who inputted no arguments
 */
void print_usage() {
	printf("Usage: imgproc <command> [<command args...>]\n");
	printf("Commands are:\n  list\n");
	printf("  exec <plugin> <input img> <output img> [<plugin args...>]\n");
}

/*
 * Destroys the list of loaded plugins
 *
 * Parameters:
 * 	plugins -  a dynamically allocated array holding all plugins
 * 	num_plug - integer containing the number of plugins in the array
 */
void destroy_list(struct Plugin **plugins, int num_plug) {
	for(int i = 0; i < num_plug; i++) {
		dlclose(plugins[i]->handle);
		free(plugins[i]);
	}

	free(plugins);
}

/*
 * Creates a full file path for dlopen
 *
 * Parameters:
 * 	folder -  char * with the directory string
 * 	filename - char * with the filename
 */
char *get_full_path(char * folder, char * filename) {
	// calculate the size of the new string
	int folder_len = strlen(folder);
	int filename_len = strlen(filename);
	int new_length = folder_len + filename_len + 2;

	// Dynamically allocate a new char * and fill it
	char * full_path = malloc(new_length);

	// Replicates a string concatenation
	for (int i = 0; i < new_length - 1; i++) {
		if (i < folder_len) {
			full_path[i] = folder[i];
		} else if (i == folder_len) {
			full_path[i] = '/';
		} else {
			full_path[i] = filename[i - folder_len - 1];
		}
	}
	
	// Append null terminator
	full_path[new_length - 1] = '\0';

	return full_path;
}

/*
 * Takes in a filename and the length of the filename and checks the last
 * three characters to make sure that the file is a .so file
 *
 * Parameters:
 * 	filename - a character string that contains the name of the file
 * 	len - an integer that holds the length of the filename
 *
 * Returns:
 * 	an integer, 0 if the file extension is incorrect, 1 if the extension
 * 		matches .so
 */
int library_file_check(char * filename, int len) {	
	char period = filename[len - 3];
	char s = filename[len - 2];
	char o = filename[len - 1];

	if (period != '.') {
		return 0;
	} else if (s != 's') {
		return 0;
	} else if (o != 'o') {
		return 0;
	} else {
		return 1;
	}
}

/*
 * Finds the index in the array of plugins for the desired plugin
 *
 * Parameters:
 * 	argv - command line input strings
 * 	plugins - array of pointers to plugin structs
 * 	num_plug - integer number of plugins in the array
 *
 * Return
 * 	an integer index of the plugin we want
 */
int find_plugin_index(char * argv[], struct Plugin **plugins, int num_plug) {	
	for (int i = 0; i < num_plug; i++) {
		if (strcmp(argv[2], plugins[i]->get_plugin_name()) == 0) {
			return i;
		}
	}

	return -1;
}

/*
 * Goes through the entries in the directory and sees if it is a
 * plugin to load into the plugins array
 *
 * Parameters:
 * 	entry - the entry in the directory
 * 	environment_list - the string to the directory 
 * 	plugins - dynamic array of plugins
 * 	num_plug - the current number of plugins in array
 * 	plugin_dir - DIR pointer to the plugin directory
 *
 * Returns:
 * 	an int error code
 */
int fill_plugins(struct dirent *entry, char* environment_list, struct Plugin**
		plugins_list, int *num_plug, DIR *plugin_dir) {
	char * filename;
	char * full_path;
	int is_library_file;
	int filename_len;

	while (entry != NULL) {
		// Overwrite the previous full path with just the directory
                // path. Then store the filename and its length
                //strcpy(full_path, environment_list);
                filename = entry->d_name;
                filename_len = strlen(filename);

                // Check if the filename is a shared library file
                is_library_file = library_file_check(filename,
                                filename_len);

                if (is_library_file == 1) {
                        // Check if the number of plugins will excede the
                        // size of the array. If so, realloc a larger array
                        if (*num_plug % 5 == 0 && *num_plug != 0) {
                                plugins_list = realloc(plugins_list,
                                                8*(*num_plug + 5));
                                if (!plugins_list) {
                                        return 5;
                                }
                        }

                        // Create a pointer to a plugin struct and store in 
                        // the plugins list array
                        full_path = get_full_path(environment_list, filename);
                        plugins_list[*num_plug] = plugin_create(full_path);
                        free(full_path);
                        (*num_plug)++;
                }

                entry = readdir(plugin_dir);
	}

	return 0;
}


/*
 * Checks the command line arguments to ensure they are valid
 *
 * Parameters:
 * 	argc - an integer with the number of command line arguments
 * 	argv - command line argument strings
 * 	plugin_list - an array of pointers to loaded plugins
 * 	num_plug - an integer holding how many plugins were loaded
 *
 * Returns:
 * 	an integer error code
 */
int check_input(int argc, char* argv[], struct Plugin **plugin_list, 
		int num_plug) {
	
	// Check that the command and number of arguments are valid
	if (strcmp(argv[1], "list") == 0 && argc > 2) {
		return 1;
	} else if (strcmp(argv[1], "list") == 0) {
		return 0;
	} else if (strcmp(argv[1], "exec") != 0) {
		return 2;
	}

	// No command requires less than 5 command line arguments
	if (argc < 5) {
		return 1;
	}
	
	// Check all Plugins functions
	for (int i = 0; i < num_plug; i++) {
		if (check_functions(plugin_list, i) != 0) {
			return 4; 
		}
	}
	
	// find the plugin in the loaded plugins that matches the desired
	// plugin from command line
	int plugin_index = find_plugin_index(argv, plugin_list, num_plug);
	
	// If no loaded plugin matched the command line plugin, throw an error
	if (plugin_index == -1) {
		return 2;
	}

	return 0;
}

/*
 * Performs the list command 
 *
 * Parameters:
 * 	plugins - an array of plugin pointers
 * 	num_plug - an integer with the number of plugins in the array
 *
 * Returns:
 * 	an integer that represents the error state
 */
int list(struct Plugin **plugins, int num_plug) {
	printf("Loaded %d plugin(s)\n", num_plug);
	for (int i = 0; i < num_plug; i++) {		
		printf("%8s: %s\n", plugins[i]->get_plugin_name(), 
				plugins[i]->get_plugin_desc());
	}

	return 0;
}

/*
 * Checks if the needed functions and plugins are loaded
 *
 * Parameters:
 * 	plugins - dynamically allocated array of plugins
 * 	target - integer index of the plugin we are interested in
 * 
 * Returns:
 * 	an error code int
 */
int check_functions(struct Plugin** plugins, int target) {
	if (NULL == plugins[target]->get_plugin_name) {
		return 4;
	} else if (NULL == plugins[target]->get_plugin_desc) {
		return 4;
	} else if (NULL == plugins[target]->parse_arguments) {
		return 4;
	} else if (NULL == plugins[target]->transform_image) {
		return 4;
	}
	

	return 0;
}

/*
 * Performs the exec command
 *
 * Parameters:
 * 	argc - integer with number of command line arguments
 * 	argv - string array of the command line arguments
 * 	plugins - a dynamically allocated array of plugin struct pointers
 * 	num_plug - the number of plugins in the array
 *
 * Returns:
 * 	an error code integer
 */ 	
int execute(int argc, char *argv[], struct Plugin **plugins, int num_plug) {
	int error = 0;

	// Gets the index of the plugin we want, does not check for errors
	int plugin_index = find_plugin_index(argv, plugins, num_plug);
	if (plugin_index == -1) {
		return 1;
	}

	// Read in the target file into an image struct
	struct Image *original_img = img_read_png(argv[3]);
	if (original_img == NULL) {
		return 1; // Implying the read is bad because of an invalid 
			  // file name
	}

	int diff = argc - 5;
	char *args[diff];
	
	// If the number of command line arguments is greater than 5, 
	// put all the string arguments after the file output name into
	// a shorter string array
	if (diff > 0) {
		for (int i = 0; i < diff; i++) {
			args[i] = argv[i + 5];
		}
	}
	
	// Get the arguments struct from parse arguments
	void *arguments = plugins[plugin_index]->parse_arguments(diff, args);
	if (arguments == NULL) {
		img_destroy(original_img);
		return 1;
	}

	// Perform the transformation on the image
	struct Image *output_img = plugins[plugin_index]->transform_image(
			original_img, arguments);
	
	// Destroy the original image struct and check if the transformation
	// was successful	
	img_destroy(original_img);
	if (output_img == NULL) {
		return 7;
	}
	
	// Write the new image to the correct filename
	error = img_write_png(output_img, argv[4]);
	
	// Destroy the original and output images
	img_destroy(output_img);
	
	// Because for some reason write returns 0 on error and 1 on success
	return !error;
}

/*
 * Prints out the correct error message
 *
 * Parameters:
 * 	error - integer error code
 */
void print_error(int error) {
	switch (error) {
		case 1:
			printf("Error: Missing/invalid command line"); 
			printf(" arguments\n");
			break;
		case 2:
			printf("Error: Unknown command name\n");
			break;
		case 3:
			printf("Error: An image processing can't be loaded\n");
			break;
		case 4:
			printf("Error: A required API function can't be"); 
			printf(" found within a loaded plugin\n");
			break;
		case 5:
			printf("Error: A memory allocation error occured\n");
			break;
		default:
			printf("Error: An error has occured\n");
	}
}

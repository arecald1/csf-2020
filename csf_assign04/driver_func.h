/*
 * Header file for functions for the driver main file to use
 * CSF Assignment 4
 * Andres Recalde
 * arecald1@jhu.edu
 */

//#include <dlfcn.h>
//#include <stdlib.h>

struct Plugin {
	void *handle; // does this need malloced?
	const char *(*get_plugin_name)(void);
	const char *(*get_plugin_desc)(void);
	void *(*parse_arguments)(int num_args, char *args[]);
	struct Image *(*transform_image)(struct Image *source, void *arg_data);
};

struct Plugin *plugin_create(const char *path);
void print_usage();
void destroy_list(struct Plugin **plugins, int num_plug);
int library_file_check(char *filename, int len);
int find_plugin_index(char *argv[], struct Plugin **plugins, int num_plug);
int fill_plugins(struct dirent *entry, char *environment_list, struct Plugin** 
		plugins_list, int *num_plug, DIR *plugin_dir);
int check_input(int argc, char* argv[], struct Plugin **plugin_list, 
		int num_plug);
int check_functions(struct Plugin** plugins, int target);
int list(struct Plugin **plugins, int num_plug);
int execute(int argc, char *argv[], struct Plugin **plugins, int num_plug);
char *get_full_path(char * folder, char * filename);
void print_error(int error);
